// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'coordenadas_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$CoordenadasController on CoordenadasControllerBase, Store {
  final _$positionStreamAtom =
      Atom(name: 'CoordenadasControllerBase.positionStream');

  @override
  StreamSubscription<dynamic>? get positionStream {
    _$positionStreamAtom.reportRead();
    return super.positionStream;
  }

  @override
  set positionStream(StreamSubscription<dynamic>? value) {
    _$positionStreamAtom.reportWrite(value, super.positionStream, () {
      super.positionStream = value;
    });
  }

  final _$CoordenadasControllerBaseActionController =
      ActionController(name: 'CoordenadasControllerBase');

  @override
  void setPositionStream(StreamSubscription<dynamic> ps) {
    final _$actionInfo = _$CoordenadasControllerBaseActionController
        .startAction(name: 'CoordenadasControllerBase.setPositionStream');
    try {
      return super.setPositionStream(ps);
    } finally {
      _$CoordenadasControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearPositionStream() {
    final _$actionInfo = _$CoordenadasControllerBaseActionController
        .startAction(name: 'CoordenadasControllerBase.clearPositionStream');
    try {
      return super.clearPositionStream();
    } finally {
      _$CoordenadasControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
positionStream: ${positionStream}
    ''';
  }
}
