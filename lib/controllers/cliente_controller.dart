import 'package:mobx/mobx.dart';
import 'package:lionpremium/models/cliente_model.dart';
part 'cliente_controller.g.dart';

class ClienteController = ClienteControllerBase with _$ClienteController;

abstract class ClienteControllerBase with Store {

  @observable
  ClienteModel? cliente;
  
  @action
  void setCliente(ClienteModel c){
    cliente = c;
  }

  @action
  void clearCliente(){
    cliente = null;
  }

}
