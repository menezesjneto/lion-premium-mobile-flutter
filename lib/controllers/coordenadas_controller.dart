import 'dart:async';

import 'package:mobx/mobx.dart';
import 'package:lionpremium/models/coordenada_model.dart';
part 'coordenadas_controller.g.dart';

class CoordenadasController = CoordenadasControllerBase with _$CoordenadasController;

abstract class CoordenadasControllerBase with Store {

  @observable
  StreamSubscription? positionStream;


  // --------------------

  @action
  void setPositionStream(StreamSubscription ps){
    positionStream = ps;
  }

  @action
  void clearPositionStream(){
    positionStream = null;
  }

}
