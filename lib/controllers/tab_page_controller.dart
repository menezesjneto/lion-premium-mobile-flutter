import 'package:flutter/widgets.dart';
import 'package:mobx/mobx.dart';
part 'tab_page_controller.g.dart';

class TabPageController = TabPageControllerBase with _$TabPageController;

abstract class TabPageControllerBase with Store {

  @observable
  int? pageIndex = 1;

  @observable
  bool? isLoadingMain = false;

  @observable
  PageController? pageController;

  @action
  void setIsLoadingMain(bool value){
    isLoadingMain = value;
  }
  
  @action
  void setPageIndex(int page){
    pageIndex = page;
  }

  @action
  void resetPageIndex(){
    pageIndex = 0;
  }

  @action
  void setTabPageController(PageController controller){
    pageController = controller;
  }
}
