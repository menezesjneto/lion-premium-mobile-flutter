// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'questionario_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$QuestionarioController on QuestionarioControllerBase, Store {
  final _$perguntasAtom = Atom(name: 'QuestionarioControllerBase.perguntas');

  @override
  ObservableList<PerguntaModel> get perguntas {
    _$perguntasAtom.reportRead();
    return super.perguntas;
  }

  @override
  set perguntas(ObservableList<PerguntaModel> value) {
    _$perguntasAtom.reportWrite(value, super.perguntas, () {
      super.perguntas = value;
    });
  }

  final _$respostasAtom = Atom(name: 'QuestionarioControllerBase.respostas');

  @override
  ObservableList<RespostaModel> get respostas {
    _$respostasAtom.reportRead();
    return super.respostas;
  }

  @override
  set respostas(ObservableList<RespostaModel> value) {
    _$respostasAtom.reportWrite(value, super.respostas, () {
      super.respostas = value;
    });
  }

  final _$questAtom = Atom(name: 'QuestionarioControllerBase.quest');

  @override
  ObservableList<Map<String, dynamic>> get quest {
    _$questAtom.reportRead();
    return super.quest;
  }

  @override
  set quest(ObservableList<Map<String, dynamic>> value) {
    _$questAtom.reportWrite(value, super.quest, () {
      super.quest = value;
    });
  }

  final _$QuestionarioControllerBaseActionController =
      ActionController(name: 'QuestionarioControllerBase');

  @override
  void clearEnableProximaPergunta() {
    final _$actionInfo =
        _$QuestionarioControllerBaseActionController.startAction(
            name: 'QuestionarioControllerBase.clearEnableProximaPergunta');
    try {
      return super.clearEnableProximaPergunta();
    } finally {
      _$QuestionarioControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setEnableProximaPergunta(bool value) {
    final _$actionInfo =
        _$QuestionarioControllerBaseActionController.startAction(
            name: 'QuestionarioControllerBase.setEnableProximaPergunta');
    try {
      return super.setEnableProximaPergunta(value);
    } finally {
      _$QuestionarioControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearPerguntas() {
    final _$actionInfo = _$QuestionarioControllerBaseActionController
        .startAction(name: 'QuestionarioControllerBase.clearPerguntas');
    try {
      return super.clearPerguntas();
    } finally {
      _$QuestionarioControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addPerguntas(PerguntaModel value) {
    final _$actionInfo = _$QuestionarioControllerBaseActionController
        .startAction(name: 'QuestionarioControllerBase.addPerguntas');
    try {
      return super.addPerguntas(value);
    } finally {
      _$QuestionarioControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearRespostas() {
    final _$actionInfo = _$QuestionarioControllerBaseActionController
        .startAction(name: 'QuestionarioControllerBase.clearRespostas');
    try {
      return super.clearRespostas();
    } finally {
      _$QuestionarioControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addRespostas(RespostaModel value) {
    final _$actionInfo = _$QuestionarioControllerBaseActionController
        .startAction(name: 'QuestionarioControllerBase.addRespostas');
    try {
      return super.addRespostas(value);
    } finally {
      _$QuestionarioControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearQuest() {
    final _$actionInfo = _$QuestionarioControllerBaseActionController
        .startAction(name: 'QuestionarioControllerBase.clearQuest');
    try {
      return super.clearQuest();
    } finally {
      _$QuestionarioControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void addQuest(Map<String, dynamic> value) {
    final _$actionInfo = _$QuestionarioControllerBaseActionController
        .startAction(name: 'QuestionarioControllerBase.addQuest');
    try {
      return super.addQuest(value);
    } finally {
      _$QuestionarioControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void updateElementQuest(List<RespostaModel> value, int index) {
    final _$actionInfo = _$QuestionarioControllerBaseActionController
        .startAction(name: 'QuestionarioControllerBase.updateElementQuest');
    try {
      return super.updateElementQuest(value, index);
    } finally {
      _$QuestionarioControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
perguntas: ${perguntas},
respostas: ${respostas},
quest: ${quest}
    ''';
  }
}
