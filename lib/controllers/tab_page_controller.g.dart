// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'tab_page_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$TabPageController on TabPageControllerBase, Store {
  final _$pageIndexAtom = Atom(name: 'TabPageControllerBase.pageIndex');

  @override
  int? get pageIndex {
    _$pageIndexAtom.reportRead();
    return super.pageIndex;
  }

  @override
  set pageIndex(int? value) {
    _$pageIndexAtom.reportWrite(value, super.pageIndex, () {
      super.pageIndex = value;
    });
  }

  final _$isLoadingMainAtom = Atom(name: 'TabPageControllerBase.isLoadingMain');

  @override
  bool? get isLoadingMain {
    _$isLoadingMainAtom.reportRead();
    return super.isLoadingMain;
  }

  @override
  set isLoadingMain(bool? value) {
    _$isLoadingMainAtom.reportWrite(value, super.isLoadingMain, () {
      super.isLoadingMain = value;
    });
  }

  final _$pageControllerAtom =
      Atom(name: 'TabPageControllerBase.pageController');

  @override
  PageController? get pageController {
    _$pageControllerAtom.reportRead();
    return super.pageController;
  }

  @override
  set pageController(PageController? value) {
    _$pageControllerAtom.reportWrite(value, super.pageController, () {
      super.pageController = value;
    });
  }

  final _$TabPageControllerBaseActionController =
      ActionController(name: 'TabPageControllerBase');

  @override
  void setIsLoadingMain(bool value) {
    final _$actionInfo = _$TabPageControllerBaseActionController.startAction(
        name: 'TabPageControllerBase.setIsLoadingMain');
    try {
      return super.setIsLoadingMain(value);
    } finally {
      _$TabPageControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setPageIndex(int page) {
    final _$actionInfo = _$TabPageControllerBaseActionController.startAction(
        name: 'TabPageControllerBase.setPageIndex');
    try {
      return super.setPageIndex(page);
    } finally {
      _$TabPageControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void resetPageIndex() {
    final _$actionInfo = _$TabPageControllerBaseActionController.startAction(
        name: 'TabPageControllerBase.resetPageIndex');
    try {
      return super.resetPageIndex();
    } finally {
      _$TabPageControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void setTabPageController(PageController controller) {
    final _$actionInfo = _$TabPageControllerBaseActionController.startAction(
        name: 'TabPageControllerBase.setTabPageController');
    try {
      return super.setTabPageController(controller);
    } finally {
      _$TabPageControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
pageIndex: ${pageIndex},
isLoadingMain: ${isLoadingMain},
pageController: ${pageController}
    ''';
  }
}
