import 'package:mobx/mobx.dart';
part 'internet_controller.g.dart';

class InternetController = InternetControllerBase with _$InternetController;

abstract class InternetControllerBase with Store {

  @observable
  bool? internetOff;
  
  @action
  void setInternetOff(bool value){
    internetOff = value;
  }

}
