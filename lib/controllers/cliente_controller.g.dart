// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'cliente_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ClienteController on ClienteControllerBase, Store {
  final _$clienteAtom = Atom(name: 'ClienteControllerBase.cliente');

  @override
  ClienteModel? get cliente {
    _$clienteAtom.reportRead();
    return super.cliente;
  }

  @override
  set cliente(ClienteModel? value) {
    _$clienteAtom.reportWrite(value, super.cliente, () {
      super.cliente = value;
    });
  }

  final _$ClienteControllerBaseActionController =
      ActionController(name: 'ClienteControllerBase');

  @override
  void setCliente(ClienteModel c) {
    final _$actionInfo = _$ClienteControllerBaseActionController.startAction(
        name: 'ClienteControllerBase.setCliente');
    try {
      return super.setCliente(c);
    } finally {
      _$ClienteControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void clearCliente() {
    final _$actionInfo = _$ClienteControllerBaseActionController.startAction(
        name: 'ClienteControllerBase.clearCliente');
    try {
      return super.clearCliente();
    } finally {
      _$ClienteControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
cliente: ${cliente}
    ''';
  }
}
