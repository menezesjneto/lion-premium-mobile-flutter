import 'package:mobx/mobx.dart';
part 'gps_controller.g.dart';

class GpsController = GpsControllerBase with _$GpsController;

abstract class GpsControllerBase with Store {

  @observable
  bool? gpsOff;
  
  @action
  void setGpsOff(bool value){
    gpsOff = value;
  }

}
