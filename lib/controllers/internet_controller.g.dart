// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'internet_controller.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$InternetController on InternetControllerBase, Store {
  final _$internetOffAtom = Atom(name: 'InternetControllerBase.internetOff');

  @override
  bool? get internetOff {
    _$internetOffAtom.reportRead();
    return super.internetOff;
  }

  @override
  set internetOff(bool? value) {
    _$internetOffAtom.reportWrite(value, super.internetOff, () {
      super.internetOff = value;
    });
  }

  final _$InternetControllerBaseActionController =
      ActionController(name: 'InternetControllerBase');

  @override
  void setInternetOff(bool value) {
    final _$actionInfo = _$InternetControllerBaseActionController.startAction(
        name: 'InternetControllerBase.setInternetOff');
    try {
      return super.setInternetOff(value);
    } finally {
      _$InternetControllerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
internetOff: ${internetOff}
    ''';
  }
}
