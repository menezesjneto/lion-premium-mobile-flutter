import 'package:mobx/mobx.dart';
import 'package:lionpremium/models/pergunta_model.dart';
import 'package:lionpremium/models/resposta_model.dart';
part 'questionario_controller.g.dart';

class QuestionarioController = QuestionarioControllerBase with _$QuestionarioController;

abstract class QuestionarioControllerBase with Store {



  @observable
  ObservableList<PerguntaModel> perguntas = ObservableList();

  @observable
  ObservableList<RespostaModel> respostas = ObservableList();

  @observable
  ObservableList<Map<String, dynamic>> quest = ObservableList();

  bool? enableProximaPergunta = true;

  @action
  void clearEnableProximaPergunta(){
    enableProximaPergunta = false;
  }

  @action
  void setEnableProximaPergunta(bool value){
    enableProximaPergunta = value;
  }

  @action
  void clearPerguntas(){
    perguntas.clear();
  }

  @action
  void addPerguntas(PerguntaModel value){
    perguntas.insert(0, value);
  }

  @action
  void clearRespostas(){
    respostas.clear();
  }

  @action
  void addRespostas(RespostaModel value){
    respostas.insert(0, value);
  }

  @action
  void clearQuest(){
    quest.clear();
  }

  @action
  void addQuest(Map<String, dynamic> value){
    quest.insert(0, value);
  }

  @action
  void updateElementQuest(List<RespostaModel> value, int index){
    quest[index]["respostas"] = value;
  }


}
