import 'dart:async';
import 'dart:convert';

import 'package:lionpremium/models/cidade_model.dart';
import 'package:lionpremium/models/cnae_model.dart';
import 'package:lionpremium/models/motivorecusa_model.dart';
import 'package:lionpremium/models/pergunta_model.dart';
import 'package:lionpremium/models/pesquisa_model.dart';
import 'package:lionpremium/models/ramo_model.dart';
import 'package:lionpremium/models/resposta_model.dart';
import 'package:lionpremium/models/situacao_model.dart';
import 'package:lionpremium/models/uf_model.dart';
import 'package:lionpremium/models/usuario_model.dart';
import 'package:lionpremium/models/info_data.dart';
import 'package:lionpremium/providers/api_provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../models/login_model.dart';


class UserProvider {
  static final UserProvider _singleton = new UserProvider._internal();
  factory UserProvider() {
    return _singleton;
  }
  UserProvider._internal();

  static Future<bool> setUserLocal(UsuarioModel alunoLogin) async {
    String aluno = json.encode(alunoLogin.toJson());
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString('aluno', aluno);
  }

  static Future<UsuarioModel?> getAlunoLocal() async {
    try {
      final SharedPreferences? prefs = await SharedPreferences.getInstance();
      String? clientString = prefs!.getString('aluno');
      var client = UsuarioModel.fromJson(json.decode(clientString!));
      return client;
    } catch (e) {
      return null;
    }
  }

  static Future<bool> setVersionAppLocal(String versaoApp) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString('versao', versaoApp);
  }

  static Future<String?> getVersionAppLocal() async {
    try {
      final SharedPreferences? prefs = await SharedPreferences.getInstance();
      String? versaoApp = prefs!.getString('versao');
      return versaoApp;
    } catch (e) {
      return null;
    }
  }

  static Future<bool> setCodigoCfcLocal(String codigocfc) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString('codigocfc', codigocfc);
  }

  static Future<String?> getCodigoCfcLocal() async {
    try {
      final SharedPreferences? prefs = await SharedPreferences.getInstance();
      String? codigocfc = prefs!.getString('codigocfc');
      return codigocfc;
    } catch (e) {
      return null;
    }
  }

  static Future<bool> removeCodigoCfc() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    return prefs.remove('codigocfc');
  }

  static Future<bool> _removeUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove('versao');
    prefs.remove('cidades');
    prefs.remove('ufs');
    prefs.remove('ramos');
    prefs.remove('cnaes');
    prefs.remove('checkMotivacao');
    prefs.remove('situacao');
    prefs.remove('pesquisas');
    prefs.remove('perguntas');
    prefs.remove('respostas');
    prefs.remove('jornada');
    prefs.remove('motivorecusa');
    return prefs.remove('aluno');
  }

  static Future<bool> setModelListLoginLocal(
      String key, List<dynamic> modelList) async {
    List<Map<String, dynamic>> list = [];
    for (var item in modelList) {
      list.add(item.toMap());
    }
    var listEncode = json.encode({key: list});
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.setString(key, listEncode);
  }

  static Future<List<SituacaoModel>?> getSituacaoListLoginLocal() async {
    final SharedPreferences? prefs = await SharedPreferences.getInstance();
    String? listString = prefs!.getString('situacao');
    if (listString != null) {
      List<SituacaoModel> list = [];
      list = SituacaoModel.listFromJson(json.decode(listString)['situacao']);
      return list;
    } else
      return null;
  }

  static Future<List<MotivoRecusaModel>?> getMotivorecusaListLoginLocal() async {
    final SharedPreferences? prefs = await SharedPreferences.getInstance();
    String? listString = prefs!.getString('motivorecusa');
    if (listString != null) {
      List<MotivoRecusaModel> list = [];
      list = MotivoRecusaModel.listFromJson(json.decode(listString)['motivorecusa']);
      return list;
    } else
      return null;
  }

  static Future<List<CidadeModel>?> getCidadesListLoginLocal() async {
    final SharedPreferences? prefs = await SharedPreferences.getInstance();
    String? listString = prefs!.getString('cidades');
    if (listString != null) {
      List<CidadeModel> list = [];
      list = CidadeModel.listFromJson(json.decode(listString)['cidades']);
      return list;
    } else
      return null;
  }

  static Future<List<RamoModel>?> getRamosListLoginLocal() async {
    final SharedPreferences? prefs = await SharedPreferences.getInstance();
    String? listString = prefs!.getString('ramos');
    if (listString != null) {
      List<RamoModel> list = [];
      list = RamoModel.listFromJson(json.decode(listString)['ramos']);
      return list;
    } else
      return null;
  }

  static Future<List<CnaeModel>?> getCnaesListLoginLocal() async {
    final SharedPreferences? prefs = await SharedPreferences.getInstance();
    String? listString = prefs!.getString('cnaes');
    if (listString != null) {
      List<CnaeModel> list = [];
      list = CnaeModel.listFromJson(json.decode(listString)['cnaes']);
      return list;
    } else
      return null;
  }

  static Future<List<PesquisaModel>?> getPesquisasListLoginLocal() async {
    final SharedPreferences? prefs = await SharedPreferences.getInstance();
    String? listString = prefs!.getString('pesquisas');
    if (listString != null) {
      List<PesquisaModel> list = [];
      list = PesquisaModel.listFromJson(json.decode(listString)['pesquisas']);
      return list;
    } else
      return null;
  }

  static Future<List<PerguntaModel>?> getPerguntasListLoginLocal() async {
    final SharedPreferences? prefs = await SharedPreferences.getInstance();
    String? listString = prefs!.getString('perguntas');
    if (listString != null) {
      List<PerguntaModel> list = [];
      list = PerguntaModel.listFromJson(json.decode(listString)['perguntas']);
      return list;
    } else
      return null;
  }

  static Future<List<RespostaModel>?> getRespostasListLoginLocal() async {
    final SharedPreferences? prefs = await SharedPreferences.getInstance();
    String? listString = prefs!.getString('respostas');
    if (listString != null) {
      List<RespostaModel> list = [];
      list = RespostaModel.listFromJson(json.decode(listString)['respostas']);
      return list;
    } else
      return null;
  }

  static Future<List<UfModel>?> getUfsListLoginLocal() async {
    final SharedPreferences? prefs = await SharedPreferences.getInstance();
    String? listString = prefs!.getString('ufs');
    if (listString != null) {
      List<UfModel> list = [];
      list = UfModel.listFromJson(json.decode(listString)['ufs']);
      return list;
    } else
      return null;
  }

  static Future<dynamic> doLogin(
      LoginModel loginModel, InfoData infoData) async {
    try {
      List<dynamic> apps = [];

      Map filter1 = infoData.toJson();

      filter1['procedure'] = 'usp_v_APPConsultorAutenticar';

      Map<dynamic, dynamic> filter = {}
        ..addAll(filter1)
        ..addAll(loginModel.toMap());

      filter['aplicativos'] = "";

      Map<String, dynamic> log = {'log': filter};

      var data = json.encode(log);

      Map<String, dynamic> queryParameters = {'concentrador': data};

      var res =
          await ApiProvider.restPost('', {}, queryParameters, 'json', null);
      
      if (res['retorno'][0]['cdRetorno'] >= 200) {
        print(res['usuario'][0]);
        UsuarioModel user;
        List<UfModel> ufs = [];
        List<CidadeModel> cidades = [];

        List<RamoModel> ramos = [];
        List<CnaeModel> cnaes = [];
        List<SituacaoModel> situacaoVisita = [];

        List<PerguntaModel> perguntas = [];
        List<PesquisaModel> pesquisas = [];
        List<RespostaModel> respostas = [];

        List<MotivoRecusaModel> motivorecusa = [];

        user = UsuarioModel.fromJson(res['usuario'][0]);
        ufs = UfModel.listFromJson(res['uf']);
        cidades = CidadeModel.listFromJson(res['cidade_proposta']);
        ramos = RamoModel.listFromJson(res['ramo']);
        cnaes = CnaeModel.listFromJson(res['cnae']);
        situacaoVisita = SituacaoModel.listFromJson(res['situacao_visita']);

        perguntas = PerguntaModel.listFromJson(res['pergunta']);
        pesquisas = PesquisaModel.listFromJson(res['pesquisa']);
        respostas = RespostaModel.listFromJson(res['resposta']);

        motivorecusa = MotivoRecusaModel.listFromJson(res['motivorecusa']);

        setUserLocal(user);

        setModelListLoginLocal("ufs", ufs);
        setModelListLoginLocal("cidades", cidades);
        setModelListLoginLocal("ramos", ramos);
        setModelListLoginLocal("cnaes", cnaes);
        setModelListLoginLocal("situacao", situacaoVisita);

        setModelListLoginLocal("pesquisas", pesquisas);
        setModelListLoginLocal("perguntas", perguntas);
        setModelListLoginLocal("respostas", respostas);

        setModelListLoginLocal("motivorecusa", motivorecusa);

        //  setModelListLoginLocal('tipoAula', tipoAula);

        //  PackageInfo packageInfo  = await PackageInfo.fromPlatform();

        //  String? versaoApp = packageInfo.version.replaceAll(".", "");

        //  UserProvider.setVersionAppLocal(versaoApp);

        //  print("versao app no login $versaoApp");

        return {'value': true, 'msgRetorno': res['retorno'][0]['msgRetorno']};
      }

      return {'value': false, 'msgRetorno': res['retorno'][0]['msgRetorno']};
    } catch (e) {
      print('UserProvider.doLogin: ${e.toString()}');
      return {
        'value': null,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }

  static void logoutUser() async {
    _removeUser();
  }

  static Future<Map> editProfile(Map infoData) async {
    try {
      UsuarioModel? user = await UserProvider.getAlunoLocal();

      infoData['idUsuario'] = user!.s01Cdid;

      Map<String, dynamic> log = {'log': infoData};

      var data = json.encode(log);

      Map<String, dynamic> queryParameters = {'concentrador': data};

      var res =
          await ApiProvider.restPost('', {}, queryParameters, 'json', null);

      if (res['retorno'][0]['cdRetorno'] >= 200) {
        UsuarioModel user = (await getAlunoLocal())!;

        user.s01Txnome = infoData['s01_txnome'];
        // user.s01Txtelefone = infoData['s01_txtelefone'];
        // user.s01Fgsexo = infoData['s01_fgsexo'] == "Masculino" ? 0 : 1;
        // user.s01Dtnascimento = infoData['s01_dtnascimento'];
        // user.s01Txemail = infoData['s01_txemail'];
        // user.s01Txidentidade = infoData['s01_txidentidade'];
        // user.s01Txexpedidor = infoData['s01_txexpedidor'];
        // user.s01Dtexpedicao = infoData['s01_dtexpedicao'];
        // user.s01Txcelular = infoData['s01_txcelular'];
        // user.s01Txwhatsapp = infoData['s01_txwhatsapp'];

        setUserLocal(user);
        print("alterado!");

        return {'value': true, 'msgRetorno': res['retorno'][0]['msgRetorno']};
      }
      return {'value': false, 'msgRetorno': res['retorno'][0]['msgRetorno']};
    } catch (e) {
      print('exception editProfile: ' + e.toString());
      return {
        'value': null,
        'msgRetorno': 'Ops, ocorreu um erro ao atualizar o seu perfil.'
      };
    }
  }

  static Future<dynamic> deleteConta(InfoData infoData) async {
    try {
      UsuarioModel? user = await UserProvider.getAlunoLocal();

      Map filter1 = infoData.toJson();

      filter1['idUsuario'] = user!.s01Cdid;

      filter1['procedure'] = 'usp_p_APPDeletarDadosAluno';

      Map<String, dynamic> log = {'log': filter1};

      var data = json.encode(log);

      Map<String, dynamic> queryParameters = {'concentrador': data};

      var res =
          await ApiProvider.restPost('', {}, queryParameters, 'json', null);

      if (res['retorno'][0]['cdRetorno'] >= 200) {
        logoutUser();

        return {'value': true, 'msgRetorno': res['retorno'][0]['msgRetorno']};
      }

      return {'value': false, 'msgRetorno': res['retorno'][0]['msgRetorno']};
    } catch (e) {
      print('AulaProvider.iniciarAula: ${e.toString()}');
      return {
        'value': null,
        'msgRetorno': 'Ops, houve um erro de comunicação com o servidor!'
      };
    }
  }



}
