import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animated_dialog/flutter_animated_dialog.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:lionpremium/widgets/theme.dart';


class CustomWidgets {

  static final CustomWidgets _singleton = new CustomWidgets._internal();
  factory CustomWidgets() {
    return _singleton;
  }
  CustomWidgets._internal();

  static Widget showLoading(context){
    return Container(
      color: Colors.grey.withOpacity(0.4),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: SizedBox(
          child: Container(
            margin: EdgeInsets.all(60.0),
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(bottom: 15.0),
                  child: SpinKitCircle(
                    color: Colors.green,
                    size: 35,
                  ),
                ),

                Text("Aguarde um instante", style: TextStyle(color: Colors.black45, fontSize: 14.0, decoration: TextDecoration.none), textAlign: TextAlign.justify),
              ]
            ),
          ),
        ),
      )
    );
  }

  static Widget showLoadingSaving(context){
    return Container(
      color: Colors.grey.withOpacity(0.4),
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: Center(
        child: SizedBox(
          child: Container(
            margin: EdgeInsets.all(60.0),
            padding: EdgeInsets.all(20.0),
            decoration: BoxDecoration(
              shape: BoxShape.rectangle,
              color: Colors.white,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text("Salvando", style: TextStyle(color: Colors.black45, fontSize: 20.0, decoration: TextDecoration.none), textAlign: TextAlign.justify),
                
                Container(
                  margin: EdgeInsets.only(top: 15.0, bottom: 15.0),
                  child: SpinKitCircle(
                    color: Colors.green,
                    size: 35,
                  ),
                ),

                Text("Aguarde um instante", style: TextStyle(color: Colors.black45, fontSize: 14.0, decoration: TextDecoration.none), textAlign: TextAlign.justify),
              ]
            ),
          ),
        ),
      )
    );
  }

  static Widget showCirularLoading(bool load){
    return load?Container():Container(
      margin: EdgeInsets.only(top: 80.0),
      alignment: Alignment.center,
      height: 50.0,
      width: 50.0,
      child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(CustomsColors.customOrange))
    );
  }

  static Widget showEmptyList(String texto){
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(top: 80.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Icon(Icons.block, size: 100.0, color: CustomsColors.customOrange),
          Text(texto, style: TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold, color: CustomsColors.customOrange),)
        ],
      )
    );
  }

  static Widget showEmptyListII(context, String texto){
    return Container(
      alignment: Alignment.center,
      //height: MediaQuery.of(context).size.height * 0.7,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Container(
            width: 300,
            child: Image.asset("assets/imgs/animation1.gif", fit: BoxFit.contain),
          ),
          Positioned(
            bottom: 0,
            child: Text("$texto", style: TextStyle(color: Colors.black54, fontSize: 22), textAlign: TextAlign.center),
          )
        ],
      ),
    );
  }

  static Widget getCircleIcon(icon){
    return Container(
      child: Icon(icon, color: Colors.black),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Colors.grey[200],
        shape: BoxShape.circle,
        boxShadow: [
          BoxShadow(
            color: Colors.grey[300]!,
            offset: Offset(0.0, 0.0),
            blurRadius: 2.0,
            spreadRadius: 1.0
          ),
        ]
      )
    );
  }

  static Widget getSquareImg(img){
    return Container(
      child: Image.asset(img),
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        color: Colors.grey[300],
        boxShadow: [
          BoxShadow(
            color: Colors.grey[300]!,
            offset: Offset(0.0, 0.0),
            blurRadius: 2.0,
            spreadRadius: 1.0
          ),
        ]
      )
    );
  }

  static Widget customFooterRefresh(bool isLoadingPage){
    return CustomFooter(
      height: isLoadingPage?55.0:0.0,
      builder: (BuildContext context, LoadStatus? mode){
        if(mode == LoadStatus.loading) return SpinKitThreeBounce(
          color: CustomsColors.customOrange,
          size: 30.0,
        );
        else return Container(
          height: 0.0,
        );
      },
    );
  }

  static Widget showInfoSistema(String texto){
    return Card(
      color: Colors.grey[200],
      margin: EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
      shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(15.0)),
      child: Padding(
        padding: EdgeInsets.all(10.0),
        child: Text(texto, style: TextStyle(fontSize: 16.0, color: Colors.black54, fontWeight: FontWeight.bold)),
      )
    );
  }

  static Widget customImagePlaceHolder(){
    return Container(
      margin: EdgeInsets.only(left: 50, right: 50, top: 10),
      alignment: Alignment.center,
      height: 120,
      width: 120,
      child: Stack(
        children: [
          Container(
            alignment: Alignment.center,
            height: 120,
            width: 120,
            decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(width: 1, color: Colors.grey)
            ),
          ),
          Container(
            alignment: Alignment.center,
            height: 121,
            margin: EdgeInsets.only(left: 30),
            width: 60,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
          ),
          Container(
            alignment: Alignment.center,
            height: 60,
            margin: EdgeInsets.only(top: 30),
            width: 121,
            decoration: BoxDecoration(
              color: Colors.white,
            ),
          ),
          // Container(
          //   alignment: Alignment.center,
          //   height: 80,
          //   width: 80,
          //   child: Icon(Icons.camera_alt, size: 70),
          //   decoration: BoxDecoration(
          //     color: Colors.transparent,
          //   ),
          //),
        ],
      )
    );
  }

  static dynamic showSnackBarSuccess(scaffoldKey, String message){
    return scaffoldKey.currentState.showSnackBar(
      SnackBar(
        content: Text(message, style: TextStyle(fontSize: 17, color: Colors.white)),
        backgroundColor: Colors.green,
        duration: Duration(seconds: 2),
        behavior: SnackBarBehavior.floating,
        padding: EdgeInsets.fromLTRB(20, 10, 10, 10),
        //margin: EdgeInsets.only(bottom: 30, left: 20, right: 20),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(20)))
      )
    );
  }

  static showDialogFuncionalidade(context) {
    showAnimatedDialog(
      context: context,
      animationType: DialogTransitionType.scaleRotate,
      curve: Curves.fastOutSlowIn,
      duration: Duration(milliseconds: 500),
      builder: (BuildContext ctx) {
        return StatefulBuilder(builder: (context, StateSetter setState2) {
        return ClassicGeneralDialogWidget(
          contentText: "Funcionalidade indisponível",
          onPositiveClick: () async{
            Navigator.pop(context);
          },
          positiveText: 'OK',
          positiveTextStyle: TextStyle(color: Colors.black),
        );
        });
      },
    );
  }

}