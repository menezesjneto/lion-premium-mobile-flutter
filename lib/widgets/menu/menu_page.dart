import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lionpremium/pages/config/ajuda_page.dart';
import 'package:lionpremium/pages/parcelas/parcelas_1_page.dart';
import 'package:lionpremium/pages/parcelas/parcelas_2_page.dart';
import 'package:lionpremium/widgets/custom_text/custom_text.dart';

class MenuPage extends StatelessWidget {
  List<Map> menus = [
    {
      'title': 'Faturas',
      'icon': FontAwesomeIcons.clipboardList,
      'page': Parcelas1Page(),
    },
    {
      'title': 'Próximas parcelas',
      'icon': FontAwesomeIcons.idCard,
      'page': Parcelas2Page(),
    },
    {
      'title': 'Parcelas pagas',
      'icon': FontAwesomeIcons.carAlt,
      'page': Parcelas1Page(),
    },
    {
      'title': 'Ajuda',
      'icon': FontAwesomeIcons.questionCircle,
      'page': AjudaPage()
    }
  ];
  @override
  Widget build(BuildContext context) {
  return Scaffold(
      backgroundColor: Colors.black.withOpacity(0.5),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 120),
          for (var item in menus) TextButton(
            onPressed: (){
              if(item['page'] != null) Navigator.of(context).push(MaterialPageRoute(
                builder: (BuildContext context) => item['page'])
              );
            },
            style: TextButton.styleFrom(
              minimumSize: Size.zero, 
              padding: EdgeInsets.zero, 
            ),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: MediaQuery.of(context).size.width * 0.1,
                      child: Icon(item['icon'], color: Colors.white, size: 25),
                    ),
                    Container(
                      width: MediaQuery.of(context).size.width * 0.6,
                      child: CustomText(text: item['title'], fontSize: 22, color: Colors.white, fontWeight: FontWeight.w400)
                    ),
                    Icon(Icons.arrow_right, color: Colors.white, size: 40),
                  ],
                ),
                Container(
                  margin: EdgeInsets.fromLTRB(40, 15, 40, 20),
                  width: MediaQuery.of(context).size.width,
                  height: 1,
                  color: Colors.white,
                ),
              ],
            ),
          ),
          SizedBox(height: 100),
          IconButton(
            icon: Icon(Icons.close, size: 35, color: Colors.white),
            onPressed: (){
              Navigator.pop(context);
            },
          ),
        ],
      ),
    );
  }
}