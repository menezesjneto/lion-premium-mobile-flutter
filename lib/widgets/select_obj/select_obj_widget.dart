import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import 'package:lionpremium/models/tipo_aula_model.dart';
import 'package:lionpremium/providers/user_provider.dart';
import '../theme.dart';


class SelectObjWidget {

  static final SelectObjWidget _singleton = new SelectObjWidget._internal();
  factory SelectObjWidget() {
    return _singleton;
  }
  SelectObjWidget._internal();



  static Widget _getEmptyList(context){
    return Container(
      margin: EdgeInsets.only(top: 70.0, bottom: 30.0),
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Icon(Icons.block, size: 20.0),
          Text(' Lista vazia!', style: TextStyle(fontSize: 18.0))
        ],
      )
    );
  }

  static Widget _getLoading(context){
    return Container(
      margin: EdgeInsets.only(top: 60.0, bottom: 30.0),
      alignment: Alignment.center,
      width: MediaQuery.of(context).size.width,
      height: 50.0,
      child: Container(
        height: 40.0,
        width:  40.0,
        child: CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(CustomsColors.customOrange))
      )
    );
  }

}
