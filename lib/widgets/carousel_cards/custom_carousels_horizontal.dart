import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lionpremium/widgets/custom_text/custom_text.dart';
import 'package:lionpremium/widgets/theme.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';

class CustomCarouselHorizontalI extends StatefulWidget {
  final List<dynamic>? cards;
  CustomCarouselHorizontalI({this.cards});

  @override
  _CustomCarouselHorizontalIState createState() => _CustomCarouselHorizontalIState();
}

class _CustomCarouselHorizontalIState extends State<CustomCarouselHorizontalI> {
  int _index = 0;

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Container(
      height: 200,
      width: width,
      alignment: Alignment.centerLeft,
      child: ListView.builder(
        itemCount: widget.cards!.length,
        scrollDirection: Axis.horizontal,
        primary: true,
        shrinkWrap: true,
        itemBuilder: (_, i) {
          //var card = widget.cards![i];
          return Container(
            margin: _index == 0 ? EdgeInsets.only(right: 20) : EdgeInsets.only(right: 5, left: 5),
            height: 151,
            width: width * 0.72,
            alignment: Alignment.center,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [

                Container(
                  height: 140,
                  margin: EdgeInsets.only(left: 20),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      CircularPercentIndicator(
                        radius: 60.0,
                        lineWidth: 5.0,
                        percent: 0.38,
                        startAngle: 180,
                        center: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            CustomText(text: 'Pago', color: Colors.grey[400], fontWeight: FontWeight.w500),
                            CustomText(text: '38%', color: CustomsColors.customOrange, fontWeight: FontWeight.bold),
                          ],
                        ),
                        progressColor: CustomsColors.customOrange,
                      ),
                      VerticalDivider(color: Colors.transparent),
                      Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(text: 'Parcelas pagas', color: Colors.grey[400], fontWeight: FontWeight.w400),
                            CustomText(text: '14 de 48', color: Colors.black, fontWeight: FontWeight.bold),
                          ],
                        ),
                      )
                    ],
                  ),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(20))
                  ),
                ),

                Container(
                  width: width,
                  height: 1,
                  margin: EdgeInsets.fromLTRB(20, 0, 20, 10),
                  color: Colors.grey,
                ),
                
                Expanded(
                  child: Container(
                    margin: EdgeInsets.fromLTRB(20, 0, 20, 0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        CustomText(text: 'Adiantar parcelas', fontWeight: FontWeight.w500, color: Colors.black,),
                        Icon(Icons.chevron_right, color: Colors.grey)
                      ],
                    ),
                  ),
                ),

              ],
            ),
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(20))
            ),
          );
        },
      ),
    );
  }
}