import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../theme.dart';

class BotaoBuscaFiltroWidget extends StatelessWidget {
  
  Function? pesquisar;
  BotaoBuscaFiltroWidget({Key? key, this.pesquisar}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      elevation: 0,
      child: Container(
        margin: EdgeInsets.fromLTRB(40, 20, 40, 20),
        height: 60.0,
        child: RaisedButton(
          color: CustomsColors.customOrange,
          disabledColor: Colors.grey[500],
          child: Text('Buscar', style: TextStyle(color: Colors.white, fontSize: 17.0),),
          shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
          onPressed: pesquisar == null ? null : () {
            
            pesquisar!();
            
          }
        ),
      ),
    );
  }

}
