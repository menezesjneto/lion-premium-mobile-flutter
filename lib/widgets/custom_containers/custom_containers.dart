import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lionpremium/widgets/custom_text/custom_text.dart';
import 'package:lionpremium/widgets/theme.dart';

class CustomContainerI extends StatelessWidget {
  final Widget? child;
  final Widget? tralingWidget;
  final bool? showResizeWidget;
  final double? width;
  final EdgeInsetsGeometry? margin;
  final EdgeInsetsGeometry? paddingTitleMenu;
  final String? titleMenu;
  final String? titleContainer;
  final String? textTrailing;
  final bool? onlyTopBorderRadius;
  final bool? onlyBottomBorderRadius;
  final bool? whitoutTopBottomBorders;
  final bool? whitoutBorders;
  final Function? callback;
  final Function? callbackLeadingWidget;

  const CustomContainerI({
    @required this.child,
    @required this.width,
    @required this.margin,
    this.paddingTitleMenu,
    this.tralingWidget,
    this.showResizeWidget,
    this.callback,
    this.titleMenu,
    this.titleContainer,
    this.textTrailing,
    this.onlyTopBorderRadius,
    this.onlyBottomBorderRadius,
    this.whitoutTopBottomBorders,
    this.whitoutBorders,
    this.callbackLeadingWidget
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: margin,
      width: width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [

          titleMenu != null ? TextButton(
            style: TextButton.styleFrom(
              alignment: Alignment.center,
              tapTargetSize: MaterialTapTargetSize.shrinkWrap,
              minimumSize: Size.zero, 
              padding: EdgeInsets.fromLTRB(0, 20, 0, 20), 
            ),
            child: Container(
              padding: paddingTitleMenu,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  CustomText(text: titleMenu, fontSize: 14, fontWeight: FontWeight.w500, color: Colors.black),
                  tralingWidget != null ? tralingWidget! :  showResizeWidget != null ?  showResizeWidget == false ? Container() :
                  _resizeWidget() :
                  Icon(FontAwesomeIcons.chevronRight, color: CustomsColors.customOrange, size: 12)
                ],
              ),
            ),
            onPressed: showResizeWidget != null ? null : callback != null ? (){
              callback!();
            } : (){},
          ) : Container(),

          titleMenu != null ? SizedBox(height: 4) : Container(),

          Container(
            width: width,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                titleContainer != null ? 
                Container(
                  child: ListTile(
                    horizontalTitleGap: 0,
                    visualDensity: VisualDensity.compact,
                    title: CustomText(text: titleContainer, fontSize: 14, fontWeight: FontWeight.w500, color: Colors.grey),
                    trailing: CustomText(text: textTrailing, fontSize: 14, color: Colors.grey, 
                      fontWeight: FontWeight.w400
                    ),
                  ),
                ) : Container(),
                child!
              ],
            ),
            decoration: BoxDecoration(
              border: whitoutBorders == true ? null : whitoutTopBottomBorders == true ? 
                Border(left: BorderSide(color: Colors.grey[500]!), right: BorderSide(color: Colors.grey[500]!)) : 
                Border.all(color: Colors.grey[500]!),
              borderRadius: whitoutTopBottomBorders == true ? null : onlyTopBorderRadius == true ? 
                BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10)) :
                onlyBottomBorderRadius == true ?
                BorderRadius.only(bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)) :
                BorderRadius.all(Radius.circular(10))
            ),
          ),
        ],
      ),
    );
  }
  Widget _resizeWidget(){
    return TextButton(
      style: TextButton.styleFrom(
        tapTargetSize: MaterialTapTargetSize.shrinkWrap,
        minimumSize: Size.zero, 
        padding: EdgeInsets.zero, 
      ),
      child: Row(
        children: [
          Icon(Icons.settings_ethernet, color: Colors.grey[900], size: 18),
          VerticalDivider(color: Colors.transparent, width: 5),
          CustomText(text: 'Organizar', fontSize: 12, color: Colors.grey[900]),
        ],
      ),
      onPressed: callbackLeadingWidget != null ? (){
        callbackLeadingWidget!();
      } : (){},
    );
  }
}

class CustomContainerDsPresentation extends StatelessWidget {
  final String? titleContainer;

  const CustomContainerDsPresentation({
    required this.titleContainer,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(5),
      margin: EdgeInsets.only(top: 20),
      width: MediaQuery.of(context).size.width,
      color: CustomsColors.customOrange,
      child: CustomText(text: titleContainer, fontWeight: FontWeight.bold, color: Colors.white, fontSize: 16)
    );
  }
}

class CustomContainerII extends StatelessWidget {
  final Size? size;
  final String? title;
  final Widget? child;

  const CustomContainerII({
    @required this.size,
    required this.title,
    required this.child
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: size!.height,
      width: size!.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(top: 20, left: 20, bottom: 20),
            child: CustomText(text: title, fontSize: 14, fontWeight: FontWeight.w500),
          ),
          Container(
            margin: EdgeInsets.only(left: 25),
            child: child!,
          ),
        ],
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        color: Colors.grey[700]
      ),
    );
  }
}

