import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:lionpremium/constants/mensagens.dart';
import 'package:lionpremium/widgets/theme.dart';


class CustomInput {
  static final CustomInput _singleton = new CustomInput._internal();
  factory CustomInput() {
    return _singleton;
  }
  CustomInput._internal();

  static InputDecoration inputDecorationI(String hintText, {bool? showError, bool? obscureText, Function? setObscureText}) {
    return InputDecoration(
      hintText: hintText,
      hintStyle: TextStyle(color: Colors.grey, fontWeight: FontWeight.w300),
      counterText: '',
      counterStyle: TextStyle(fontSize: 0),
      //counter: null,
      floatingLabelBehavior: FloatingLabelBehavior.always,
      focusedBorder: OutlineInputBorder(borderSide: new BorderSide(color: Colors.grey), borderRadius: BorderRadius.circular(5.0)),
      enabledBorder: OutlineInputBorder(borderSide: new BorderSide(color: Colors.transparent), borderRadius: BorderRadius.circular(5.0)),
      errorBorder: OutlineInputBorder(borderSide: new BorderSide(color: Colors.grey),  borderRadius: BorderRadius.circular(5.0)),
      focusedErrorBorder: OutlineInputBorder(borderSide: new BorderSide(color: Colors.grey), borderRadius: BorderRadius.circular(5.0)),
      errorStyle: TextStyle(fontSize: showError == true ? 10.0 : 0.0),
      contentPadding: EdgeInsets.all(15.0),
      suffixIcon: setObscureText != null ? IconButton(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 15.0, 0.0),
        icon: Icon(
          obscureText == true ? FontAwesomeIcons.solidEye : FontAwesomeIcons.solidEyeSlash,
          color: CustomsColors.customOrange,
          size: 25.0
        ),
        onPressed: () {
          setObscureText();
        }
      ): null
    );
  }

  static InputDecoration inputDecorationII(String hintText, {bool? showError, bool? obscureText, Function? setObscureText}) {
    return InputDecoration(
      hintText: hintText,
      hintStyle: TextStyle(color: Colors.grey, fontSize: 12),
      
      counterText: '',
      counterStyle: TextStyle(fontSize: 0),
      //counter: null,
      floatingLabelBehavior: FloatingLabelBehavior.always,
      focusedBorder: InputBorder.none,
      enabledBorder: InputBorder.none,
      errorBorder: OutlineInputBorder(borderSide: new BorderSide(color: Colors.grey),  borderRadius: BorderRadius.circular(5.0)),
      focusedErrorBorder: InputBorder.none,
      errorStyle: TextStyle(fontSize: showError == true ? 10.0 : 0.0),
      contentPadding: EdgeInsets.all(15.0),
      suffixIcon: setObscureText != null ? IconButton(
        padding: EdgeInsets.fromLTRB(0.0, 0.0, 15.0, 0.0),
        icon: Icon(
          obscureText == true ? FontAwesomeIcons.solidEye : FontAwesomeIcons.solidEyeSlash,
          color: CustomsColors.customOrange,
          size: 25.0
        ),
        onPressed: () {
          setObscureText();
        }
      ): null
    );
  }

  static InputDecoration inputDecorationSelect(String hintText) {
    return InputDecoration(
      hintText: hintText,
      hintStyle: TextStyle(color: Colors.grey),
      counterText: '',
      counterStyle: TextStyle(fontSize: 0),
      counter: null,
      floatingLabelBehavior: FloatingLabelBehavior.always,
      focusedBorder: OutlineInputBorder(borderSide: new BorderSide(color: Colors.grey), borderRadius: BorderRadius.circular(5.0)),
      enabledBorder: OutlineInputBorder(borderSide: new BorderSide(color: CustomsColors.customOrange), borderRadius: BorderRadius.circular(5.0)),
      errorBorder: OutlineInputBorder(borderSide: new BorderSide(color: Colors.grey),  borderRadius: BorderRadius.circular(5.0)),
      focusedErrorBorder: OutlineInputBorder(borderSide: new BorderSide(color: Colors.grey), borderRadius: BorderRadius.circular(5.0)),
      contentPadding: EdgeInsets.all(15.0),
      suffixIcon: Icon(Icons.keyboard_arrow_down_outlined),
    );
  }

  static BoxDecoration decorationCircular() {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(15.0)
    );
  }

  static BoxDecoration decorationCircularII() {
    return BoxDecoration(
      color: Colors.grey[200],
      borderRadius: BorderRadius.circular(10.0),
    );
  }

  static BoxDecoration decorationCircularIII() {
    return BoxDecoration(
      color: CustomsColors.customOrange,
      borderRadius: BorderRadius.circular(10.0),
    );
  }

  static Widget getInputLabel(textLabel) {
    return Container(
      margin: EdgeInsets.only(left: 30.0, bottom: 10.0, top: 0.0),
      child: Text(textLabel, style: TextStyle(fontSize: 18.0, color: Colors.white, fontWeight: FontWeight.w300))
    );
  }

  static Widget getInputLabelII(textLabel) {
    return Container(
      margin: EdgeInsets.only(left: 15.0, bottom: 5.0, top: 15.0),
      child: Text(textLabel, style: TextStyle(fontSize: 18.0, color: Colors.black))
    );
  }

  static Widget getInputLabelIII(textLabel) {
    return Container(
      alignment: Alignment.center,
      margin: EdgeInsets.only(bottom: 5.0, top: 15.0),
      child: Text(textLabel, style: TextStyle(fontSize: 12.0, color: Colors.black, fontWeight: FontWeight.bold))
    );
  }

  static void showDialogMensagem(context, value) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(Mensagens[value]!['titulo']!),
          content: ((Mensagens[value]!['mensagem'] != '') ? Text(Mensagens[value]!['mensagem']!) : null),
          actions: <Widget>[
            new FlatButton(
              child: new Text("OK",  style: TextStyle( fontWeight: FontWeight.bold, fontSize: 20.0, color: Colors.blue)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  static void showDialogCustomMensagem(context, value) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text(value['titulo']),
          content: ((value['mensagem'] == '') ? Text(value['mensagem']) : null),
          actions: <Widget>[
            FlatButton(
              child: new Text("OK",  style: TextStyle( fontWeight: FontWeight.bold, fontSize: 20.0, color: Colors.blue)),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  static void showSnackbarSuccessMessage(scaffoldKey, value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      behavior: SnackBarBehavior.fixed,
      content: Text(value, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white)),
      backgroundColor: Colors.blueAccent,
      duration: Duration(seconds: 2),
    ));
  }

  static void showSnackbarErrorMessage(scaffoldKey, value) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      behavior: SnackBarBehavior.fixed,
      content: Text(value, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.black)),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 3),
    ));
  }

  static void showSnackbarErrorCustomMessage(scaffoldKey, message) {
    scaffoldKey.currentState.showSnackBar(SnackBar(
      behavior: SnackBarBehavior.fixed,
      content: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text("Erro!", style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18, color: Colors.white)),
          Text("$message", style: TextStyle(fontSize: 16, color: Colors.white)),
        ],
      ),
      backgroundColor: Colors.redAccent,
      duration: Duration(seconds: 3),
    ));
  }

}
