import 'package:flutter/material.dart';


class CustomText extends StatelessWidget {
  final String? text;
  final double? fontSize;
  final Color? color;
  final FontStyle? fontStyle;
  final FontWeight? fontWeight;

  CustomText({
    @required this.text,
    this.fontSize,
    this.color,
    this.fontStyle,
    this.fontWeight,
  });

  @override
  Widget build(BuildContext context) {
    return Text(text!,
      style: TextStyle(
          fontSize: fontSize,
          fontWeight: fontWeight != null ? fontWeight : FontWeight.normal,
          fontStyle: fontStyle != null ? fontStyle : FontStyle.normal,
          color: color,
      ),
    );
  }
  
}
