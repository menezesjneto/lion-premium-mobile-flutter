class PerguntaModel {
  int? id;
  int? r16Fgtipo;
  int? r16Cdpesquisa;
  int? r16Fgvalidacao;
  int? r16Boopcional;
  int? r16BocomFoto;
  int? r16Boinformativo;
  int? r16Nrminimo;
  int? r16Nrmaximo;
  int? r16Ordem;
  String? r16Txpergunta;
  String? r16Txdescritivo;
  String? r16Txpergunta1;
  String? r16TxcaminhoFoto;

  int? r16BoGaleria;

  PerguntaModel(
      {this.id,
      this.r16Txpergunta,
      this.r16Fgtipo,
      this.r16Cdpesquisa,
      this.r16Fgvalidacao,
      this.r16Boopcional,
      this.r16BocomFoto,
      this.r16Boinformativo,
      this.r16Nrminimo,
      this.r16Nrmaximo,
      this.r16Txdescritivo,
      this.r16Txpergunta1,
      this.r16TxcaminhoFoto,
      this.r16Ordem,
      this.r16BoGaleria,
      });

  PerguntaModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    r16Txpergunta = json['r16_txpergunta'];
    r16Fgtipo = json['r16_fgtipo'];
    r16Cdpesquisa = json['r16_cdpesquisa'];
    r16Fgvalidacao = json['r16_fgvalidacao'];
    r16Boopcional = json['r16_boopcional'];
    r16BocomFoto = json['r16_bocomFoto'];
    r16Boinformativo = json['r16_boinformativo'];
    r16Nrminimo = json['r16_nrminimo'];
    r16Nrmaximo = json['r16_nrmaximo'];
    r16Txdescritivo = json['r16_txdescritivo'];
    r16Txpergunta1 = json['r16_txpergunta1'];
    r16TxcaminhoFoto = json['r16_txcaminhoFoto'];
    r16BoGaleria = json['r16_bogaleria'];
    r16Ordem = json['r16_ordem'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['r16_txpergunta'] = this.r16Txpergunta;
    data['r16_fgtipo'] = this.r16Fgtipo;
    data['r16_cdpesquisa'] = this.r16Cdpesquisa;
    data['r16_fgvalidacao'] = this.r16Fgvalidacao;
    data['r16_boopcional'] = this.r16Boopcional;
    data['r16_bocomFoto'] = this.r16BocomFoto;
    data['r16_boinformativo'] = this.r16Boinformativo;
    data['r16_nrminimo'] = this.r16Nrminimo;
    data['r16_nrmaximo'] = this.r16Nrmaximo;
    data['r16_txdescritivo'] = this.r16Txdescritivo;
    data['r16_txpergunta1'] = this.r16Txpergunta1;
    data['r16_txcaminhoFoto'] = this.r16TxcaminhoFoto;
    data['r16_ordem'] = this.r16Ordem;
    data['r16_bogaleria'] = this.r16BoGaleria;
    return data;
  }

  static List<PerguntaModel> listFromJson(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => PerguntaModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<PerguntaModel> data) {
    if(data == null) return [];
    else return data.map((post) => post.toMap()).toList();
  }


}