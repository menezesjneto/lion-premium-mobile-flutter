import 'package:lionpremium/models/uf_model.dart';
import 'package:lionpremium/utils/extractor_json.dart';

class ClienteModel {
  int? d03Cdid;
  int? d03Cdcidade;
  int? d03Fgtipopessoa;
  int? idCnae;
  int? d03CdsituacaoCadastral;
  String? d03Txcnpj;
  String? dtcadastro;
  String? txendereco;
  String? txcidadeUF;
  String? txcontato;
  String? d03Nmdescricao;
  String? d03TxpessoaContato;
  String? d03TxpessoaEmail;
  String? d03Nmendereco;
  String? d03Nrendereco;
  String? d03Txcomplemento;
  String? d03Nrtelefone;
  String? d03Nrtelefoneopc;
  String? d03Cep;
  String? d03Latitude;
  String? d03Longitude;
  String? txDisplay;
  String? d03Nome;
  String? responsavel;
  String? bairro;
  String? logradouro;
  String? numero;
  String? txEndereco;

  //AUX
  int? idAux;
  bool? isLoading;
  int? userId;
  int? isSync;
  UfModel? ufSelected;

  String? txCidade;
  String? cnae;
  String? ramo;

  bool? isSearchLocal;

  ClienteModel(
      {this.d03Cdid,
      this.dtcadastro,
      this.txendereco,
      this.txcidadeUF,
      this.txcontato,
      this.d03Cdcidade,
      this.d03Txcnpj,
      this.d03Fgtipopessoa,
      this.d03Nmdescricao,
      this.d03TxpessoaContato,
      this.d03TxpessoaEmail,
      this.d03Nmendereco,
      this.d03Nrendereco,
      this.d03Txcomplemento,
      this.d03Nrtelefone,
      this.d03Nrtelefoneopc,
      this.d03Cep,
      this.d03Latitude,
      this.txDisplay,
      this.d03Nome,
      this.bairro,
      this.logradouro,
      this.numero,
      this.responsavel,
      this.idCnae,
      this.txEndereco,
      this.idAux,
      this.isLoading,
      this.userId,
      this.d03CdsituacaoCadastral,
      this.isSync,
      this.ufSelected,
      this.txCidade,
      this.cnae,
      this.ramo,
      this.isSearchLocal,
      this.d03Longitude});

  ClienteModel.fromJson(Map<String, dynamic> json) {
    d03Cdid = Extractor.extractInt(json['d03_cdid']);
    //d03Cdcidade = Extractor.extractInt(json['d03_cdcidade']);
    d03Fgtipopessoa = Extractor.extractInt(json['d03_fgtipopessoa']);
    idCnae = Extractor.extractInt(json['idCnae']);
    userId = Extractor.extractInt(json['userId']);
    d03CdsituacaoCadastral = Extractor.extractInt(json['d03_cdsituacaoCadastral']);
    dtcadastro = json['dtcadastro'];
    txcidadeUF = json['txcidadeUF'];
    txcontato = json['txcontato'];
    d03Txcnpj = json['d03_txcnpj'];
    d03Nmdescricao = json['d03_nmdescricao'];
    d03TxpessoaContato = json['d03_txpessoaContato'];
    d03TxpessoaEmail = json['d03_txpessoaEmail'];
    d03Nmendereco = json['d03_nmendereco'];
    d03Nrendereco = json['d03_nrendereco'];
    d03Txcomplemento = json['d03_txcomplemento'];
    d03Nrtelefone = json['d03_nrtelefone'];
    d03Nrtelefoneopc = json['d03_nrtelefoneopc'];
    d03Cep = json['d03_cep'];
    d03Latitude = json['d03_latitude'];
    d03Longitude = json['d03_longitude'];
    txDisplay = json['txDisplay'];
    d03Nome = json['d03_nome'];
    txEndereco = Extractor.extractString(json['txEndereco'], "");

    d03Nome = json['nome'];
    //d03Txcnpj = json['cpfcnpj'];
    d03TxpessoaEmail = json['email'];
    d03Cdcidade = Extractor.extractInt(json['idCidade']);
    txCidade = Extractor.extractString(json['txCidade'], '');
    d03Cep = Extractor.extractString(json['cep'], '');
    numero = Extractor.extractString(json['numero'], '');
    ufSelected = new UfModel();
    ufSelected!.id = json['idUF'];
    ufSelected!.descricao = json['txUF'];
    logradouro = Extractor.extractString(json['logradouro'], '');
    d03Txcomplemento = Extractor.extractString(json['complemento'], '');
    d03Nrtelefone = json['telefone1'];
    d03Nrtelefoneopc = json['telefone2'];
    d03Fgtipopessoa = Extractor.extractInt(json['tipopessoa']);
    bairro = Extractor.extractString(json['bairro'], '');
    responsavel = json['responsavel'];
    isLoading = false;
    idCnae = Extractor.extractInt(json['idCnae']);
    cnae = Extractor.extractString(json['txCnae'], "");
    ramo = Extractor.extractString(json['txRamo'], ""); 
    isSearchLocal = true;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['d03_cdid'] = this.d03Cdid;
    data['dtcadastro'] = this.dtcadastro;
    data['txcidadeUF'] = this.txcidadeUF;
    data['txcontato'] = this.txcontato;
    data['d03_cdcidade'] = this.d03Cdcidade;
    data['d03_txcnpj'] = this.d03Txcnpj;
    data['d03_fgtipopessoa'] = this.d03Fgtipopessoa;
    data['d03_nmdescricao'] = this.d03Nmdescricao;
    data['d03_txpessoaContato'] = this.d03TxpessoaContato;
    data['d03_txpessoaEmail'] = this.d03TxpessoaEmail;
    data['d03_nmendereco'] = this.d03Nmendereco;
    data['d03_nrendereco'] = this.d03Nrendereco;
    data['d03_txcomplemento'] = this.d03Txcomplemento;
    data['d03_nrtelefone'] = this.d03Nrtelefone;
    data['d03_nrtelefoneopc'] = this.d03Nrtelefoneopc;
    data['d03_cep'] = this.d03Cep;
    data['d03_latitude'] = this.d03Latitude;
    data['d03_longitude'] = this.d03Longitude;
    data['txDisplay'] = this.txDisplay;
    data['d03_nome'] = this.d03Nome;
    data['userId'] = this.userId;
    data['d03_cdsituacaoCadastral'] = this.d03CdsituacaoCadastral;
    data['txEndereco'] = this.txEndereco;
    return data;
  }

  //CADASTRO DE CLIENTE
  ClienteModel.fromJsonCadCliente(Map<String, dynamic> json) {
    idAux = json['idaux'];
    d03Nome = json['nome'];
    d03Txcnpj = json['cpfcnpj'];
    d03TxpessoaEmail = json['email'];
    d03Cdcidade = Extractor.extractInt(json['cidade']);
    d03Cep = json['cep'];
    numero = json['numero'];
    logradouro = json['logradouro'];
    d03Txcomplemento = json['complemento'];
    d03Nrtelefone = json['telefone1'];
    d03Nrtelefoneopc = json['telefone2'];
    d03Fgtipopessoa = Extractor.extractInt(json['tipopessoa']);
    bairro = json['bairro'];
    responsavel = json['responsavel'];
    isLoading = false;
    idCnae = Extractor.extractInt(json['idCnae']);
  }

  Map<String, dynamic> toJsonCadCliente() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['idaux'] = this.idAux;
    data['nome'] = this.d03Nome;
    data['cpfcnpj'] = this.d03Txcnpj;
    data['email'] = this.d03TxpessoaEmail;
    data['cidade'] = this.d03Cdcidade.toString();
    data['cep'] = this.d03Cep;
    data['numero'] = this.numero;
    data['logradouro'] = this.logradouro;
    data['complemento'] = this.d03Txcomplemento;
    data['telefone1'] = this.d03Nrtelefone;
    data['telefone2'] = this.d03Nrtelefoneopc;
    data['tipopessoa'] = this.d03Fgtipopessoa.toString();
    data['bairro'] = this.bairro;
    data['responsavel'] = this.responsavel;
    data['idCnae'] = this.idCnae.toString();
    data['idCliente'] = this.d03Cdid == null ? 0 : this.d03Cdid;

    return data;
  }

  static List<ClienteModel> listFromJson(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => ClienteModel.fromJson(post)).toList();
  }

  static List<ClienteModel> listFromJsonOff(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => ClienteModel.fromJsonCadCliente(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<ClienteModel> data) {
    if(data == null) return [];
    else return data.map((post) => post.toJson()).toList();
  }


}