import 'dart:io';

import 'package:lionpremium/models/tipo_aula_model.dart';



class AulaModel {
  int? p01Cdid;
  int? p01Fgsituacao;
  bool? p01BoSaida;
  bool? p01BoEntrada;
  String? r09Nmdescricao;
  String? p01TxCampo;
  String? p01DtEntrada;
  String? p01DtSaida;
  String? imagemEntrada;
  String? imagemSaida;

  //HISTORICO
  String? p01HoraEntrada;
  String? p01HoraSaida;
  String? p01Txsituacao;

  //FILTRO
  TipoAulaModel? tipoAula;
  String? dataInicial;
  String? dataFinal;
  
  //AUX
  File? myImage1;
  File? myImage2;
  bool? isLoading1;
  bool? isLoading2;
  bool? showCameraOptions;

  AulaModel(
    {
      this.p01Cdid,
      this.r09Nmdescricao,
      this.p01TxCampo,
      this.p01BoEntrada,
      this.p01DtEntrada,
      this.p01BoSaida,
      this.p01DtSaida,
      this.p01Fgsituacao,
      this.isLoading1,
      this.isLoading2,
      this.myImage1,
      this.myImage2,
      this.showCameraOptions,
      this.imagemEntrada,
      this.imagemSaida,

      //HISTORICO
      this.p01HoraEntrada,
      this.p01HoraSaida,
      this.p01Txsituacao,

      //FILTRO
      this.dataInicial,
      this.dataFinal,
      this.tipoAula,
    }
  );

  AulaModel.fromJson(Map<String, dynamic> json) {
    p01Cdid = json['p01_cdid'];
    r09Nmdescricao = json['r09_nmdescricao'];
    p01TxCampo = json['p01_txCampo'];
    p01BoEntrada = json['p01_boEntrada'] == 0 ? false : true;
    p01DtEntrada = json['p01_dtEntrada'];
    p01BoSaida = json['p01_boSaida'] == 0 ? false : true;
    p01DtSaida = json['p01_dtSaida'];
    p01Fgsituacao = json['p01_fgsituacao'];

    isLoading1 = false;
    isLoading2 = false;
  }

  AulaModel.fromJsonHistorico(Map<String, dynamic> json) {
    p01Cdid = json['p01_cdid'];
    r09Nmdescricao = json['r09_nmdescricao'];
    p01TxCampo = json['p01_txCampo'];
    p01BoEntrada = json['p01_boEntrada'] == 0 ? false : true;
    p01DtEntrada = json['p01_dtEntrada'];
    p01BoSaida = json['p01_boSaida'] == 0 ? false : true;
    p01DtSaida = json['p01_dtSaida'];
    p01Txsituacao = json['p01_txtsituacao'];

    isLoading1 = false;
    isLoading2 = false;
    imagemEntrada = "";
    imagemSaida = "";
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['p01_cdid'] = this.p01Cdid;
    data['r09_nmdescricao'] = this.r09Nmdescricao;
    data['p01_txCampo'] = this.p01TxCampo;
    data['p01_horaEntrada'] = this.p01HoraEntrada;
    data['p01_horaSaida'] = this.p01HoraSaida;
    data['p01_fgsituacao'] = this.p01Fgsituacao;
    return data;
  }

  Map<String, dynamic> toMapFiltro() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['dtInicial'] = this.dataInicial;
    data['dtFinal'] = this.dataFinal;
    data['fgTipoAula'] = this.tipoAula!.d13Cdid;
    return data;
  }

  static List<AulaModel> listFromJson(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => AulaModel.fromJson(post)).toList();
  }

  static List<AulaModel> listFromJsonHistorico(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => AulaModel.fromJsonHistorico(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<AulaModel> data) {
    if(data == null) return [];
    else return data.map((post) => post.toMap()).toList();
  }

}