class ParcelaModel {
  String? valor;
  String? vencimento;
  String? numero;
  String? data;
  bool? atrasado;

    ParcelaModel(
      {
        this.valor,
        this.vencimento,
        this.numero,
        this.data,
        this.atrasado
      }
    );

}
