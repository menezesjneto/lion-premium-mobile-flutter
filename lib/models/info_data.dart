class InfoData {

  //Dados surface
  String? idNotify;
  double? latitude;
  double? longitude;
  double? altura; 
  double? comprimento; 
  int? versao; 
  int? bateria;
  String? fgapp;


  //Dados de hardware
  String? modelo; 
  String? api; 


  InfoData({
    this.longitude,
    this.latitude,
    this.altura,
    this.api,
    this.comprimento,
    this.fgapp,
    this.idNotify,
    this.modelo,
    this.versao,
    this.bateria
  });



  Map<String, dynamic> toJson() => {
    "api": api,//
    "modelo": modelo,//
    "versao": versao,//
    "imei": "",//
    "iccid": "",//
    "bateria": bateria,
    "txLongitude": longitude,
    "txLatitude": latitude,
    "idNotify": idNotify,
    "altura": altura,
    "comprimento": comprimento,
    "fgapp": 0,
    'densidade': 2,
    
  };

}