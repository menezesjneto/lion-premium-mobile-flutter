import 'package:lionpremium/utils/extractor_json.dart';

class SituacaoModel {
  
  int? d50Cdid;
  String? d50Nmdescricao; 
  

  SituacaoModel({
    this.d50Cdid,
    this.d50Nmdescricao,
  });

  factory SituacaoModel.fromJson(dynamic json) => SituacaoModel(
    d50Cdid: json['d50_cdid'],
    d50Nmdescricao: Extractor.extractString(json['d50_nmdescricao'], ''),
  );

  Map<String, dynamic> toMap() => { 
    'd50_cdid': d50Cdid,
    'd50_nmdescricao': d50Nmdescricao,
  };


  static List<SituacaoModel> listFromJson(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => SituacaoModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<SituacaoModel> data) {
    if(data == null) return [];
    else return data.map((post) => post.toMap()).toList();
  }

}