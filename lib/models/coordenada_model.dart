class CoordenadaModel {
  int? id;
  int? userId;
  String? latitude;
  String? longitude;

  CoordenadaModel({this.userId, this.latitude, this.longitude, this.id});

  CoordenadaModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    userId = json['userId'];
    latitude = json['latitude'];
    longitude = json['longitude'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['userId'] = this.userId;
    data['latitude'] = this.latitude;
    data['longitude'] = this.longitude;
    return data;
  }

  static List<CoordenadaModel> listFromJson(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => CoordenadaModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<CoordenadaModel> data) {
    if(data == null) return [];
    else return data.map((post) => post.toMap()).toList();
  }


}