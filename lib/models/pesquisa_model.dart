class PesquisaModel {
  int? id;
  String? p13Nmpesquisa;
  String? dataPesquisa;

  PesquisaModel({this.id, this.p13Nmpesquisa, this.dataPesquisa});

  PesquisaModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    p13Nmpesquisa = json['p13_nmpesquisa'];
    dataPesquisa = json['dataPesquisa'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['p13_nmpesquisa'] = this.p13Nmpesquisa;
    data['dataPesquisa'] = this.dataPesquisa;
    return data;
  }

  static List<PesquisaModel> listFromJson(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => PesquisaModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<PesquisaModel> data) {
    if(data == null) return [];
    else return data.map((post) => post.toMap()).toList();
  }


}