
class UfModel {

  int? id;
  String? descricao;

  UfModel({
    this.id,
    this.descricao,
  });

  factory UfModel.fromJson(Map<String, dynamic> json) => new UfModel(
    id: json["id"],
    descricao: json["descricao"],
  );

  Map<String, dynamic> toMap() => {
    "id": id!,
    "descricao": descricao,
  };

  static List<UfModel> listFromJson(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => UfModel.fromJson(post)).toList();
  }

}