class MotivoRecusaModel {
  int? id;
  String? descricao;

  MotivoRecusaModel(
    {
      this.id,
      this.descricao,
    }
  );

  factory MotivoRecusaModel.fromJson(Map<String, dynamic> json) => new MotivoRecusaModel(
        id: json["id"],
        descricao: json["descricao"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "descricao": descricao,
      };

  static List<MotivoRecusaModel> listFromJson(List<dynamic> data) {
    if (data == null)
      return [];
    else
      return data.map((post) => MotivoRecusaModel.fromJson(post)).toList();
  }
}
