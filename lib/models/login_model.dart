
class LoginModel {

  String? cpf;
  String? senha;
  String? email;

  LoginModel({
    this.cpf,
    this.senha,
    this.email
  });

  factory LoginModel.fromJson(Map<String, dynamic> json) => new LoginModel(
    cpf: json["login"],
    senha: json["senha"],
  );

  Map<String, dynamic> toMap() => {
    "cpf": cpf!.replaceAll(".", ""),
    "matricula": senha,
    "email": email
  };

}