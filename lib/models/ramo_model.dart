class RamoModel {
  int? d42Cdid;
  int? d42Bosituacao;
  int? d42Boexcluido;
  int? d42Fgtipopessoa;
  String? d42Nmdescricao;

  RamoModel(
      {this.d42Cdid,
      this.d42Bosituacao,
      this.d42Nmdescricao,
      this.d42Boexcluido,
      this.d42Fgtipopessoa});

  factory RamoModel.fromJson(Map<String, dynamic> json) => new RamoModel(
        d42Cdid: json["d42_cdid"],
        d42Bosituacao: json["d42_bosituacao"],
        d42Nmdescricao: json["d42_nmdescricao"],
        d42Boexcluido: json["d42_boexcluido"],
        d42Fgtipopessoa: json["d42_fgtipopessoa"],
      );

  Map<String, dynamic> toMap() => {
        "d42_cdid": d42Cdid,
        "d42_bosituacao": d42Bosituacao,
        "d42_nmdescricao": d42Nmdescricao,
        "d42_boexcluido": d42Boexcluido,
        "d42_fgtipopessoa": d42Fgtipopessoa,
      };

  static List<RamoModel> listFromJson(List<dynamic> data) {
    if (data == null)
      return [];
    else
      return data.map((post) => RamoModel.fromJson(post)).toList();
  }
}
