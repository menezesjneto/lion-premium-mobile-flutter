class CnaeModel {
  int? d43Cdid;
  int? d43Cdramo;
  String? d43Nmdescricao;

  CnaeModel({this.d43Cdid, this.d43Cdramo, this.d43Nmdescricao});

  factory CnaeModel.fromJson(Map<String, dynamic> json) => new CnaeModel(
        d43Cdid: json["d43_cdid"],
        d43Cdramo: json["d43_cdramo"],
        d43Nmdescricao: json["d43_nmdescricao"],
      );

  Map<String, dynamic> toMap() => {
        "d43_cdid": d43Cdid,
        "d43_cdramo": d43Cdramo,
        "d43_nmdescricao": d43Nmdescricao
      };

  static List<CnaeModel> listFromJson(List<dynamic> data) {
    if (data == null)
      return [];
    else
      return data.map((post) => CnaeModel.fromJson(post)).toList();
  }
}
