
class CidadeModel {

  int? id;
  int? idUf;
  String? descricao;

  CidadeModel({
    this.id,
    this.idUf,
    this.descricao
  });

  factory CidadeModel.fromJson(Map<String, dynamic> json) => new CidadeModel(
    id: json["id"],
    idUf: json["id_uf"],
    descricao: json["descricao"],
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "id_uf": idUf,
    "descricao": descricao
  };

  static List<CidadeModel> listFromJson(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => CidadeModel.fromJson(post)).toList();
  }

}