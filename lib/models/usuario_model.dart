class UsuarioModel {
  int? s01Cdid;
  int? d01Nrddd;
  int? d05Cdid;
  String? d05Nmdescricao;
  String? d02Nmdescricao;
  int? d03Cdid;
  int? s01Nrmatricula;
  String? s01TxCpf;
  String? s01Dtcadastro;
  String? s01Txnome;
  String? d02Latitude;
  String? d02Longitude;
  String? d03Latitude;
  String? d03Longitude;
  String? dataBD;
  int? situcaoUser;
  int? d05BoJornada;
  int? d05BoMonitoramento;
  int? d05BoAplicativo;
  String? d05HoraInicioMonitoramento;
  String? d05HorafinalMonitoramento;
  int? d01Cdid;
  int? d02Cdid;
  int? d04Cdid;
  int? s01FgexigeCliente;
  int? s01Fgodometro;
  String? senhaEmail;
  int? s01Cdpdvagencia;
  int? boFotoEntrada;
  int? boFotoSaida;
  int? s01NrFotoVenda;
  int? s01FgexigePesquisa;
  int? s01BoFotoOdometro;

  UsuarioModel(
      {this.s01Cdid,
      this.d01Nrddd,
      this.d05Cdid,
      this.d05Nmdescricao,
      this.d02Nmdescricao,
      this.d03Cdid,
      this.s01Nrmatricula,
      this.s01TxCpf,
      this.s01Dtcadastro,
      this.s01Txnome,
      this.d02Latitude,
      this.d02Longitude,
      this.d03Latitude,
      this.d03Longitude,
      this.dataBD,
      this.situcaoUser,
      this.d05BoJornada,
      this.d05BoMonitoramento,
      this.d05BoAplicativo,
      this.d05HoraInicioMonitoramento,
      this.d05HorafinalMonitoramento,
      this.d01Cdid,
      this.d02Cdid,
      this.d04Cdid,
      this.s01FgexigeCliente,
      this.s01Fgodometro,
      this.s01Cdpdvagencia,
      this.boFotoEntrada,
      this.boFotoSaida,
      this.s01NrFotoVenda,
      this.s01FgexigePesquisa,
      this.s01BoFotoOdometro,
      this.senhaEmail});

  UsuarioModel.fromJson(Map<String, dynamic> json) {
    s01Cdid = json['s01_cdid'];
    d01Nrddd = json['d01_nrddd'];
    d05Cdid = json['d05_cdid'];
    d05Nmdescricao = json['d05_nmdescricao'];
    d02Nmdescricao = json['d02_nmdescricao'];
    d03Cdid = json['d03_cdid'];
    s01Nrmatricula = json['s01_nrmatricula'];
    s01TxCpf = json['s01_txcpf'];
    s01Dtcadastro = json['s01_dtcadastro'];
    s01Txnome = json['s01_txnome'];
    d02Latitude = json['d02_latitude'].toString();
    d02Longitude = json['d02_longitude'].toString();
    d03Latitude = json['d03_latitude'].toString();
    d03Longitude = json['d03_longitude'].toString();
    dataBD = json['dataBD'];
    situcaoUser = json['situcaoUser'];
    d05BoJornada = json['d05_boJornada'];
    d05BoMonitoramento = json['d05_boMonitoramento'];
    d05BoAplicativo = json['d05_boAplicativo'];
    d05HoraInicioMonitoramento = json['d05_horaInicioMonitoramento'];
    d05HorafinalMonitoramento = json['d05_horafinalMonitoramento'];
    d01Cdid = json['d01_cdid'];
    d02Cdid = json['d02_cdid'];
    d04Cdid = json['d04_cdid'];
    s01FgexigeCliente = json['s01_fgexigeCliente'];
    s01Fgodometro = json['s01_fgodometro'];
    s01Cdpdvagencia = json['s01_cdpdvagencia'];
    senhaEmail = json['senhaEmail'];
    boFotoEntrada = json['boFotoEntrada'];
    boFotoSaida = json['boFotoSaida'];
    s01NrFotoVenda = json['s01_nrFotoVenda'];
    s01FgexigePesquisa = json['s01_fgexigePesquisa'];
    s01BoFotoOdometro = json['s01_boFotoOdometro'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['s01_cdid'] = this.s01Cdid;
    data['d01_nrddd'] = this.d01Nrddd;
    data['d05_cdid'] = this.d05Cdid;
    data['d05_nmdescricao'] = this.d05Nmdescricao;
    data['d02_nmdescricao'] = this.d02Nmdescricao;
    data['d03_cdid'] = this.d03Cdid;
    data['s01_nrmatricula'] = this.s01Nrmatricula;
    data['s01_txcpf'] = this.s01TxCpf;
    data['s01_dtcadastro'] = this.s01Dtcadastro;
    data['s01_txnome'] = this.s01Txnome;
    data['d02_latitude'] = this.d02Latitude;
    data['d02_longitude'] = this.d02Longitude;
    data['d03_latitude'] = this.d03Latitude;
    data['d03_longitude'] = this.d03Longitude;
    data['dataBD'] = this.dataBD;
    data['situcaoUser'] = this.situcaoUser;
    data['d05_boJornada'] = this.d05BoJornada;
    data['d05_boMonitoramento'] = this.d05BoMonitoramento;
    data['d05_boAplicativo'] = this.d05BoAplicativo;
    data['d05_horaInicioMonitoramento'] = this.d05HoraInicioMonitoramento;
    data['d05_horafinalMonitoramento'] = this.d05HorafinalMonitoramento;
    data['d01_cdid'] = this.d01Cdid;
    data['d02_cdid'] = this.d02Cdid;
    data['d04_cdid'] = this.d04Cdid;
    data['s01_fgexigeCliente'] = this.s01FgexigeCliente;
    data['s01_fgodometro'] = this.s01Fgodometro;
    data['senhaEmail'] = this.senhaEmail;
    data['s01_cdpdvagencia'] = this.s01Cdpdvagencia;
    data['boFotoEntrada'] = this.boFotoEntrada;
    data['boFotoSaida'] = this.boFotoSaida;
    data['s01_nrFotoVenda'] = this.s01NrFotoVenda;
    data['s01_fgexigePesquisa'] = this.s01FgexigePesquisa;
    data['s01_boFotoOdometro'] = this.s01BoFotoOdometro;
    return data;
  }

  static List<UsuarioModel> listFromJson(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => UsuarioModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<UsuarioModel> data) {
    if(data == null) return [];
    else return data.map((post) => post.toJson()).toList();
  }


}