class RespostaModel {
  int? id;
  int? r17Cdpergunta;
  int? r17Cddependencia;
  String? r17Txresposta;

  //AUX
  //TIPO 0
  bool? selected = false;

  //TIPO 1
  int? respostaNumberInt = 0;

  //TIPO 2
  double? respostaNumberDouble = 0;

  //TIPO 3
  String? respostaString1 = '';

  //TIPO 4
  String? respostaString4 = '';

  //TIPO 5
  bool? selected5 = false;

  //TIPO 6
  String? respostaString6 = '';

  //TIPO 7
  String? respostaString7 = '';

  //TIPO 8
  String? respostaString8 = '';

  //TIPO 9
  String? respostaString9 = '';

  RespostaModel({
    this.id, 
    this.r17Cdpergunta, 
    this.r17Txresposta, 
    this.r17Cddependencia,

    this.selected,
    this.respostaNumberInt,
    this.respostaNumberDouble,
    this.respostaString1,
    this.respostaString4,
    this.selected5,
    this.respostaString6,
    this.respostaString7,
    this.respostaString8,
    this.respostaString9
  });

  RespostaModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    r17Cdpergunta = json['r17_cdpergunta'];
    r17Txresposta = json['r17_txresposta'];
    r17Cddependencia = json['r17_cddependencia'];
  }

  Map<String, dynamic> toMap() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['r17_cdpergunta'] = this.r17Cdpergunta;
    data['r17_txresposta'] = this.r17Txresposta;
    data['r17_cddependencia'] = this.r17Cddependencia;
    return data;
  }

  static List<RespostaModel> listFromJson(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => RespostaModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<RespostaModel> data) {
    if(data == null) return [];
    else return data.map((post) => post.toMap()).toList();
  }


}