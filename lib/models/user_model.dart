import 'package:lionpremium/utils/extractor_json.dart';

class UserModel {
  
  int? id;
  String? login;

  UserModel({
    this.id,
    this.login,
  });

  factory UserModel.fromJson(dynamic json) => UserModel(
    id: json['id'],
    login: Extractor.extractString(json['login'], '')
  );

  Map<String, dynamic> toMap() => { 
    'id': id,
    'login': login
  };

}