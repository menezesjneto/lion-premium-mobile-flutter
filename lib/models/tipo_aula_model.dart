import 'package:lionpremium/utils/extractor_json.dart';

class TipoAulaModel {
  
  int? d13Cdid;
  String? d13Nmdescricao; 
  

  TipoAulaModel({
    this.d13Cdid,
    this.d13Nmdescricao,
  });

  factory TipoAulaModel.fromJson(dynamic json) => TipoAulaModel(
    d13Cdid: json['d13_cdid'],
    d13Nmdescricao: Extractor.extractString(json['d13_nmdescricao'], ''),
  );

  Map<String, dynamic> toMap() => { 
    'd13_cdid': d13Cdid,
    'd13_nmdescricao': d13Nmdescricao,
  };


  static List<TipoAulaModel> listFromJson(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => TipoAulaModel.fromJson(post)).toList();
  }

  static List<Map<String, dynamic>> listToMap(List<TipoAulaModel> data) {
    if(data == null) return [];
    else return data.map((post) => post.toMap()).toList();
  }

}