import 'package:lionpremium/models/cliente_model.dart';

class VisitaModel {

  int? id;
  int? idSituacao;
  int? isEntrada;
  String? logradouro;
  String? observacao;
  
  int? idCliente;
  int? isSync;
  int? idCnae;
  int? tipopessoa;
  int? idCidade;
  String? nome;
  String? cpfcnpj;
  String? email;
  String? cep;
  String? numero;
  String? complemento;
  String? telefone1;
  String? telefone2;
  String? bairro;
  String? responsavel;
  String? imagem;
  String? imagemSaida;
  dynamic questionario;
  String? dtAgenda;
  String? dtVisita;
  int? motivorecusa;
  String? txFotovenda;

  //LISTAGEM VISITAS
  int? r22Cdid;
  int? r22CdsituacaoVisita;
  String? txSituacao;
  String? txCLiente;
  String? txDisplay;
  String? r22TxData;
  String? r22TxHora;

  //AUX
  ClienteModel? cliente;

  //FILTER
  String? periodo;

  VisitaModel({
    this.id,
    this.isEntrada,
    this.logradouro,
    this.observacao,
    this.idSituacao,
    this.bairro,
    this.cep,
    this.idCidade,
    this.complemento,
    this.cpfcnpj,
    this.email,
    this.idCliente,
    this.idCnae,
    this.imagem,
    this.isSync,
    this.nome,
    this.numero,
    this.questionario,
    this.responsavel,
    this.telefone1,
    this.telefone2,
    this.tipopessoa,
    this.cliente,
    this.dtAgenda,
    this.dtVisita,
    this.motivorecusa,
    this.imagemSaida,
    this.txFotovenda,

    this.r22Cdid,
    this.r22CdsituacaoVisita,
    this.r22TxData,
    this.r22TxHora,
    this.txCLiente,
    this.txDisplay,
    this.txSituacao,

    this.periodo
  });

  factory VisitaModel.fromJson(Map<String, dynamic> json) => new VisitaModel(
    id: json['id'],
    bairro: json['bairro'],
    isEntrada: json['isEntrada'],
    logradouro: json['logradouro'],
    observacao: json['observacao'],
    idSituacao: json['idSituacao'],
    cep: json['cep'],
    idCidade: json['idCidade'],
    complemento: json['complemento'],
    cpfcnpj: json['cpfcnpj'],
    email: json['email'],
    idCliente: json['idCliente'],
    idCnae: json['idCnae'],
    imagem: json['imagem'],
    isSync: json['isSync'],
    nome: json['nome'],
    numero: json['numero'],
    questionario: json['questionario'],
    responsavel: json['responsavel'],
    telefone1: json['telefone1'],
    telefone2: json['telefone2'],
    tipopessoa: json['tipopessoa'],
    dtAgenda: json['dtAgenda'],
    dtVisita: json['dtVisita'],
  );

   factory VisitaModel.fromJsonII(Map<String, dynamic> json) => new VisitaModel(
    r22Cdid: json['r22_cdid'],
    r22CdsituacaoVisita: json['r22_cdsituacaoVisita'],
    txSituacao: json['txSituacao'],
    txCLiente: json['txCLiente'],
    txDisplay: json['txDisplay'],
    r22TxData: json['r22_txData'],
    r22TxHora: json['r22_txHora'],
  );

  Map<String, dynamic> toMap() => {
    "id": id,
    "bairro": bairro,
    "isEntrada": isEntrada,
    "logradouro": logradouro,
    "observacao": observacao,
    "idSituacao": idSituacao,
    "cep": cep,
    "idCidade": idCidade,
    "complemento": complemento,
    "cpfcnpj": cpfcnpj,
    "email": email,
    "idCliente": idCliente,
    "idCnae": idCnae,
    "isSync": isSync,
    "nome": nome,
    "numero": numero,
    "questionario": questionario,
    "responsavel": responsavel,
    "telefone1": telefone1,
    "telefone2": telefone2,
    "tipopessoa": tipopessoa,
    "imagem": imagem,
    "dtAgenda": dtAgenda,
    "dtVisita": dtVisita,
    "motivorecusa": motivorecusa,
    "txFotovenda": txFotovenda,

    'r22_cdid':this.r22Cdid,
    'r22_cdsituacaoVisita': this.r22CdsituacaoVisita,
    'txSituacao': this.txSituacao,
    'txCLiente': this.txCLiente,
    'txDisplay': this.txDisplay,
    'r22_txData': this.r22TxData,
    'r22_txHora': this.r22TxHora,
  };

  Map<String, dynamic> toMapII() => {
    'r22_cdid':this.r22Cdid,
    'r22_cdsituacaoVisita': this.r22CdsituacaoVisita,
    'txSituacao': this.txSituacao,
    'txCLiente': this.txCLiente,
    'txDisplay': this.txDisplay,
    'r22_txData': this.r22TxData,
    'r22_txHora': this.r22TxHora,
  };

  static List<VisitaModel> listFromJson(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => VisitaModel.fromJson(post)).toList();
  }

  static List<VisitaModel> listFromJsonII(List<dynamic> data) {
    if(data == null) return [];
    else return data.map((post) => VisitaModel.fromJsonII(post)).toList();
  }

}