import 'package:lionpremium/pages/splash_page.dart';
import 'package:flutter/material.dart';

import 'package:lionpremium/pages/intro/intro_page.dart';
import 'package:lionpremium/pages/not_found_page.dart';


import '../pages/home_page.dart';
import '../pages/login/login_page.dart';
final routesApp = {
  '/': (BuildContext context) => new SplashPage(),
  '/home': (BuildContext context) => new HomePage(),
  '/intro': (BuildContext context) => new IntroPage(),
  '/login': (BuildContext context) => new LoginPage(),
  '/erro': (BuildContext context) => new NotFoundPage(),
};
