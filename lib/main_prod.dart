import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:lionpremium/controllers/cliente_controller.dart';
import 'package:lionpremium/controllers/coordenadas_controller.dart';
import 'package:lionpremium/controllers/gps_controller.dart';
import 'package:lionpremium/controllers/questionario_controller.dart';
import 'package:lionpremium/controllers/tab_page_controller.dart';
import 'package:lionpremium/resources/app_config.dart';
import 'package:flutter/material.dart';
import 'main.dart';
import 'services/navigation_service.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  GetIt getIt = GetIt.I;
  getIt.registerSingleton<GpsController>((GpsController()));
  getIt.registerSingleton<TabPageController>((TabPageController()));
  getIt.registerSingleton<QuestionarioController>((QuestionarioController()));
  getIt.registerSingleton<CoordenadasController>((CoordenadasController()));
   getIt.registerSingleton<ClienteController>((ClienteController()));

  Stream <bool> locationEventStream;
  final gpsCtlr = GetIt.I.get<GpsController>();

  // var configureApp = AppConfig(
  //   buildType: 'prod',
  //   child: materialApp,
  //   urlServer: '201.33.25.40:33',
  // );
  
  var configureApp = AppConfig(
    buildType: 'prod',
    child: materialApp,
    urlServer: '201.33.21.62',
  );
  

  return runApp(configureApp);
}