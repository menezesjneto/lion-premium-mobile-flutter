import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get_it/get_it.dart';
import 'package:lionpremium/controllers/gps_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lionpremium/models/parcela_model.dart';
import 'package:lionpremium/pages/parcelas/parcelas_2_page.dart';
import 'package:lionpremium/widgets/carousel_cards/custom_carousels_horizontal.dart';
import 'package:lionpremium/widgets/custom_text/custom_text.dart';
import 'package:lionpremium/widgets/menu/menu_page.dart';
import 'package:lionpremium/widgets/theme.dart';

import '../widgets/theme.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> with AutomaticKeepAliveClientMixin {
  
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final gpsCtlr = GetIt.I.get<GpsController>();

  int? tabIndex = 0;

  List<ParcelaModel> proximasParcelas = [
    ParcelaModel(
      data: '13/09/2021',
      numero: '16º',
      valor: r'R$ 2.356,14',
      vencimento: '17/11/2021',
      atrasado: true
    ),
    ParcelaModel(
      data: '13/09/2021',
      numero: '16º',
      valor: r'R$ 376,14',
      vencimento: '17/11/2021'
    ),
    ParcelaModel(
      data: '13/09/2021',
      numero: '16º',
      valor: r'R$ 4.776,14',
      vencimento: '17/11/2021'
    ),
    ParcelaModel(
      data: '13/09/2021',
      numero: '16º',
      valor: r'R$ 1.476,14',
      vencimento: '17/11/2021'
    ),
  ];
  List<ParcelaModel> parcelasPagas = [
    ParcelaModel(
      data: '13/09/2021',
      numero: '16º',
      valor: r'R$ 1.476,14',
      vencimento: '17/11/2021'
    ),
    ParcelaModel(
      data: '13/09/2021',
      numero: '16º',
      valor: r'R$ 1.476,14',
      vencimento: '17/11/2021'
    ),
    ParcelaModel(
      data: '13/09/2021',
      numero: '16º',
      valor: r'R$ 1.476,14',
      vencimento: '17/11/2021'
    ),
    ParcelaModel(
      data: '13/09/2021',
      numero: '16º',
      valor: r'R$ 1.476,14',
      vencimento: '17/11/2021'
    ),
  ];


  @override
  void initState() {
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;
 

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return WillPopScope(
      onWillPop: () async{
        return false;
      },
      child: Scaffold(
        key: _scaffoldKey,
        backgroundColor: Colors.grey[200],
        body: CustomScrollView(
          slivers: <Widget>[
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  
                  _appBar(height, width),

                  _parcelasWidget(),

                  SizedBox(height: 30),

                  _alert(),

                  _tabBar(),

                  _listTabs(),

                ]
              )
            )
          ]
        )
      ),
    );
  }

  Widget _alert(){
    return Stack(
      children: [
        Container(
          height: 170,
          width: MediaQuery.of(context).size.width,
          alignment: Alignment.bottomCenter,
          color: Colors.transparent,
          margin: EdgeInsets.only(left: 20, right: 20),
          child: Container(
            height: 150,
            width: MediaQuery.of(context).size.width,
            padding: EdgeInsets.only(left: 10),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Icon(Icons.error, color: Colors.red),
                    VerticalDivider(color: Colors.transparent, width: 5),
                    CustomText(text: 'Parcela em atraso!', color: Colors.red, fontWeight: FontWeight.bold, fontSize: 18),
                  ],
                ),
                SizedBox(height: 10),
                CustomText(text: 'Parcela 15', color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16),
                SizedBox(height: 10),
                Container(
                  width: MediaQuery.of(context).size.width * 0.5,
                  child: CustomText(text: 'Opa, parece que o pagamento da sua parcela 15 está atrasada. Apólice\n000000000094554', color: Colors.black, fontWeight: FontWeight.w300, fontSize: 14),
                  
                ),
              ],
            ),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(Radius.circular(20))
              ),
            ), 
          ),
          Positioned(
            right: 30,
            top: 0,
            child: Column(
              children: [
                Container(
                  height: 110,
                  width: 110,
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage('assets/imgs/pin.png')
                    ),
                    color: Colors.transparent
                  ),
                ),
                TextButton(
                  onPressed: () {},
                  style: ButtonStyle(
                    minimumSize: MaterialStateProperty.all<Size>(Size.zero),
                    padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.fromLTRB(20, 5, 20, 5)),
                    backgroundColor: MaterialStateProperty.all<Color>(CustomsColors.customOrange),
                    overlayColor: MaterialStateProperty.all<Color>(Colors.black),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(100.0),
                        )
                      )
                  ),
                  child: Text(
                    'PAGAR AGORA',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 12,
                      fontWeight: FontWeight.bold
                    ),
                  ),
                ),
              ],
            ),
          ),
      ]
    );
  }

  Widget _tabBar(){
    return DefaultTabController(
      length: 2,
      child: Container(
        margin: EdgeInsets.only(top: 20),
        padding: EdgeInsets.all(10),
        color: Colors.white,
        child: TabBar(
          indicatorColor: Colors.lightGreenAccent[400],
          indicatorWeight: 5,
          onTap: (index){
            setState(() {
              tabIndex = index;
            });
          },
          tabs: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 10),
              child: CustomText(text: "Próxima parcelas", fontWeight: FontWeight.bold, color: tabIndex == 0 ? Colors.black : Colors.grey[400], fontSize: 16),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 10),
              child: CustomText(text: "Parcelas pagas", fontWeight: FontWeight.bold, color: tabIndex == 1 ? Colors.black : Colors.grey[400], fontSize: 16),
            ),
          ],
        ),
      ),
    );
  }

  Widget _listTabs(){
    return Container(
      padding: EdgeInsets.only(top: 20),
      color: Colors.white,
      child: tabIndex == 0 ? _createListProximasParcelas(context) : _createListParcelasPagas(context),
    );
  }

  Widget _parcelasWidget() {
    return Container(
      margin: EdgeInsets.only(left: 20, right: 20),
      child: Column(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.centerRight,
            child: TextButton(
              style: TextButton.styleFrom(
                minimumSize: Size.zero,
                padding: EdgeInsets.zero,
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Icon(Icons.cached, color: Colors.black),
                  SizedBox(width: 5),
                  CustomText(text: 'Atualizar', color: Colors.black, fontWeight: FontWeight.w500)
                ],
              ),
              onPressed: (){},
            ),
          ),
          CustomCarouselHorizontalI(cards: ['teste', 'teste', 'teste'])
        ],
      ),
    );
  }

  Widget _appBar(height, width){
    return Container(
      height: height * 0.15,
      width: width,
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.only(top: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            height: height * 0.1,
            width: width * 0.65,
            color: Colors.yellow,
            child: FlutterLogo(),
          ),
          VerticalDivider(color: Colors.transparent),
          Container(
            child: TextButton(
              child: Icon(Icons.menu, color: Colors.black, size: 60),
              onPressed: (){
                Navigator.of(context).push(PageRouteBuilder(
                  opaque: false,
                  pageBuilder: (BuildContext context, _, __) => MenuPage()));
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _createListProximasParcelas(context) {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      itemCount: proximasParcelas.length,
      itemBuilder: (BuildContext context, int index) {
        var item = proximasParcelas[index];
        return AnimationConfiguration.staggeredList(
          position: index,
          duration: const Duration(milliseconds: 375),
          child: SlideAnimation(
            horizontalOffset: 50.0,
            child: FadeInAnimation(
              child: TextButton(
                onPressed: (){},
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.2,
                          child: Column(
                            children: [
                              CustomText(text: item.data!, fontSize: 12, color: Colors.grey[400], fontWeight: FontWeight.w500),
                              Container(
                                height: 50,
                                width: 50,
                                alignment: Alignment.center,
                                child: CustomText(text: item.numero!, fontSize: 16, color: Colors.lightGreenAccent[400], fontWeight: FontWeight.w600),
                                margin: EdgeInsets.only(top: 10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.grey[200]
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.5,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CustomText(text: item.valor!, fontSize: 16, color: Colors.black, fontWeight: FontWeight.w500),
                              SizedBox(height: 20),
                              CustomText(text: 'Boleto disponível até ' + item.vencimento!, fontSize: 12, color: Colors.grey[400], fontWeight: FontWeight.w600),
                            ],
                          ),
                        ),
                        Row(
                          children: [
                            TextButton(
                              onPressed: item.atrasado == true ? () {
                                Navigator.of(context).push(MaterialPageRoute(
                                  builder: (BuildContext context) => Parcelas2Page())
                                );
                              } : null,
                              style: ButtonStyle(
                                minimumSize: MaterialStateProperty.all<Size>(Size.zero),
                              ),
                              child: item.atrasado == true ? Text(
                                'Adiantar',
                                style: TextStyle(
                                  color:  Colors.lightGreenAccent[400],
                                  fontSize: 12,
                                  fontWeight: FontWeight.bold
                                ),
                              ): Container(),
                            ),
                            Icon(Icons.chevron_right, color: Colors.grey[400]),
                          ],
                        ),
                        
                      ],
                    ),

                    Container(
                      margin: EdgeInsets.all(30),
                      width: MediaQuery.of(context).size.width,
                      height: 1,
                      color: Colors.grey[400],
                    ),
                  ],
                ),
              ),
            )
          )
        );
      }
    );
  }

  Widget _createListParcelasPagas(context) {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      padding: EdgeInsets.fromLTRB(0.0, 10.0, 0.0, 0.0),
      itemCount: parcelasPagas.length,
      itemBuilder: (BuildContext context, int index) {
        var item = parcelasPagas[index];
        return AnimationConfiguration.staggeredList(
          position: index,
          duration: const Duration(milliseconds: 375),
          child: SlideAnimation(
            horizontalOffset: 50.0,
            child: FadeInAnimation(
              child: TextButton(
                onPressed: (){},
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width * 0.2,
                          child: Column(
                            children: [
                              CustomText(text: item.data!, fontSize: 12, color: Colors.grey[400], fontWeight: FontWeight.w500),
                              Container(
                                height: 50,
                                width: 50,
                                alignment: Alignment.center,
                                child: CustomText(text: item.numero!, fontSize: 16, color: Colors.lightGreenAccent[400], fontWeight: FontWeight.w600),
                                margin: EdgeInsets.only(top: 10),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(Radius.circular(10)),
                                  color: Colors.grey[200]
                                ),
                              )
                            ],
                          ),
                        ),
                        Container(
                          width: MediaQuery.of(context).size.width * 0.6,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              CustomText(text: item.valor!, fontSize: 16, color: Colors.black, fontWeight: FontWeight.w500),
                              SizedBox(height: 20),
                              CustomText(text: 'Boleto disponível até ' + item.vencimento!, fontSize: 12, color: Colors.grey[400], fontWeight: FontWeight.w600),
                            ],
                          ),
                        ),
                        Icon(Icons.chevron_right, color: Colors.grey[400]),
                      ],
                    ),

                    Container(
                      margin: EdgeInsets.all(30),
                      width: MediaQuery.of(context).size.width,
                      height: 1,
                      color: Colors.grey[400],
                    ),
                  ],
                ),
              ),
            )
          )
        );
      }
    );
  }

}
