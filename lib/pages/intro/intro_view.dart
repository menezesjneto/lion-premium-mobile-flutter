import 'package:lionpremium/widgets/theme.dart';
import 'package:flutter/material.dart';

import '../../widgets/theme.dart';

class IntroView extends StatelessWidget {
    
  final String texto;
  final String? img;
  IntroView({Key? key, required this.texto, this.img}) : super(key: key);
  
  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      alignment: Alignment.center,
      padding: EdgeInsets.only(top: 150.0, bottom: 90.0),
      child: Column(
        children: [

          Spacer(),
        

          Text(
            texto,
            style: TextStyle(
              color: CustomsColors.customOrange,
              fontSize: 32,
            ),
            textAlign: TextAlign.center,
          ),

          Spacer(),
        ],
      ),
    );
  }
}