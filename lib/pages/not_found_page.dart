import 'package:flutter/material.dart';
import 'package:lionpremium/widgets/theme.dart';

class NotFoundPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center( 
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 50,
              width: 50,
              child: Image.asset("assets/imgs/car_animation.gif", fit: BoxFit.cover,),
            ),
            Text('Funcionalidade em\ndesenvolvimento',
              style: TextStyle(fontSize: 22, color: CustomsColors.customBlack),
              textAlign: TextAlign.center,
            ),
          ],
        ),
      ),
    );
  }
}
