import 'package:get_it/get_it.dart';
import 'package:lionpremium/controllers/gps_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lionpremium/widgets/custom_text/custom_text.dart';
import 'package:lionpremium/widgets/menu/menu_page.dart';


class Parcelas1Page extends StatefulWidget {
  @override
  _Parcelas1PageState createState() => _Parcelas1PageState();
}

class _Parcelas1PageState extends State<Parcelas1Page> with AutomaticKeepAliveClientMixin {
  
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final gpsCtlr = GetIt.I.get<GpsController>();

  int? tabIndex = 0;

  @override
  void initState() {
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;
 

  @override
  Widget build(BuildContext context) {
    super.build(context);
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[200],
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate(
              [
                
                _appBar(height, width),

                Container(
                  alignment: Alignment.centerLeft,
                  child: IconButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    icon: Icon(FontAwesomeIcons.chevronCircleLeft, color: Colors.grey[500]),
                  ),
                ),

                Container(
                  height: MediaQuery.of(context).size.height,
                  color: Colors.white,
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        child: CustomText(text: '1 de 24', fontWeight: FontWeight.bold, color: Colors.black, fontSize: 18),
                      ),
                      SizedBox(height: 50),
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(left: 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(text: r'R$ 2.356,14', fontSize: 24, color: Colors.black, fontWeight: FontWeight.w500),
                            SizedBox(height: 10),
                            CustomText(text: 'Pago em 24/08/2021', fontSize: 20, color: Colors.grey[400], fontWeight: FontWeight.w600),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
                

              ]
            )
          )
        ]
      )
    );
  }

  Widget _appBar(height, width){
    return Container(
      height: height * 0.15,
      width: width,
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.only(top: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            height: height * 0.1,
            width: width * 0.65,
            color: Colors.yellow,
            child: FlutterLogo(),
          ),
          VerticalDivider(color: Colors.transparent),
          Container(
            child: TextButton(
              child: Icon(Icons.menu, color: Colors.black, size: 60),
              onPressed: (){
                Navigator.of(context).push(PageRouteBuilder(
                  opaque: false,
                  pageBuilder: (BuildContext context, _, __) => MenuPage()));
              },
            ),
          ),
        ],
      ),
    );
  }
}
