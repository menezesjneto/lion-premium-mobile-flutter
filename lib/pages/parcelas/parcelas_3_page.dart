import 'package:get_it/get_it.dart';
import 'package:lionpremium/controllers/gps_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lionpremium/pages/home_page.dart';
import 'package:lionpremium/pages/intro/sliding_widget.dart';
import 'package:lionpremium/widgets/custom_text/custom_text.dart';
import 'package:lionpremium/widgets/menu/menu_page.dart';

class Parcelas3Page extends StatefulWidget {
  @override
  _Parcelas3PageState createState() => _Parcelas3PageState();
}

class _Parcelas3PageState extends State<Parcelas3Page> with SingleTickerProviderStateMixin  {
  
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  AnimationController? _animationController;

  final gpsCtlr = GetIt.I.get<GpsController>();

  int? tabIndex = 0;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1700),
    )..forward();
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;
 

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[200],
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate(
              [
                
                _appBar(height, width),

                Container(
                  alignment: Alignment.centerLeft,
                  child: IconButton(
                    onPressed: (){
                      Navigator.pop(context);
                    },
                    icon: Icon(FontAwesomeIcons.chevronCircleLeft, color: Colors.grey[500]),
                  ),
                ),

                Container(
                  height: MediaQuery.of(context).size.height,
                  color: Colors.white,
                  child: Column(
                    children: [
                      Container(
                        padding: EdgeInsets.only(top: 10),
                        child: CustomText(text: 'Adiantar Parcela', fontWeight: FontWeight.bold, color: Colors.black, fontSize: 18),
                      ),
                      SizedBox(height: 350),
                      
                      Container(
                        alignment: Alignment.centerLeft,
                        margin: EdgeInsets.only(left: 20),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            CustomText(text: 'Adiantamento solicitado', fontSize: 24, color: Colors.black, fontWeight: FontWeight.w600),
                            SizedBox(height: 10),
                            CustomText(text: 'Acesse o boleto no aplicativo em\naproximadamente 30 minutos.', fontSize: 20, color: Colors.grey[400], fontWeight: FontWeight.w300),
                          ],
                        ),
                      ),
                      SizedBox(height: 20),
                      TextButton(
                        onPressed: () async{
                          Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (BuildContext context) => HomePage())
                          );
                        },
                        style: ButtonStyle(
                          padding: MaterialStateProperty.all<EdgeInsets>(EdgeInsets.all(15)),
                          foregroundColor: MaterialStateProperty.all<Color>(Colors.yellow),
                          backgroundColor: MaterialStateProperty.all<Color>(Colors.lightGreenAccent[400]!),
                          overlayColor: MaterialStateProperty.all<Color>(Colors.black),
                          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                            RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(10.0),
                            )
                          )
                        ),
                        child: FadingSlidingWidget(
                          animationController: _animationController,
                          child: AnimatedContainer(
                            duration: const Duration(seconds: 1),
                            alignment: Alignment.center,
                            width: width * 0.75,
                            height: height * 0.055,
                            child: Text(
                              'Entendi',
                              style: TextStyle(
                                color: Colors.black,
                                fontSize: width * 0.05,
                                fontWeight: FontWeight.bold
                              ),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                

              ]
            )
          )
        ]
      )
    );
  }

  Widget _appBar(height, width){
    return Container(
      height: height * 0.15,
      width: width,
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.only(top: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            height: height * 0.1,
            width: width * 0.65,
            color: Colors.yellow,
            child: FlutterLogo(),
          ),
          VerticalDivider(color: Colors.transparent),
          Container(
            child: TextButton(
              child: Icon(Icons.menu, color: Colors.black, size: 60),
              onPressed: (){
                Navigator.of(context).push(PageRouteBuilder(
                  opaque: false,
                  pageBuilder: (BuildContext context, _, __) => MenuPage()));
              },
            ),
          ),
        ],
      ),
    );
  }
}
