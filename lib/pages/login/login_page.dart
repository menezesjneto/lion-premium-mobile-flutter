import 'package:cnpj_cpf_formatter/cnpj_cpf_formatter.dart';
import 'package:cpfcnpj/cpfcnpj.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lionpremium/pages/home_page.dart';
import 'package:lionpremium/pages/intro/sliding_widget.dart';
import 'package:package_info/package_info.dart';
import 'package:lionpremium/models/info_data.dart';
import 'package:lionpremium/models/login_model.dart';
import 'package:lionpremium/widgets/custom_input.dart';
import 'package:lionpremium/widgets/theme.dart';
import 'package:url_launcher/url_launcher.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => new _LoginPageState();
}

class _LoginPageState extends State<LoginPage> with SingleTickerProviderStateMixin  {

  AnimationController? _animationController;
  
  TextEditingController cpfController = new TextEditingController();
  TextEditingController senhaController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();

  final _scaffoldKey = GlobalKey<ScaffoldState>();
  final _formKey = GlobalKey<FormState>();

  final FocusNode _loginFocus = FocusNode();
  final FocusNode _senhaFocus = FocusNode();

  InfoData infoData = new InfoData();

  LoginModel loginData = new LoginModel(
    cpf: '',
    senha: '',
    email: ''
  );

  List<dynamic> options = ['Telemetria', 'Fotografia'];

  bool sending = false;  

  PackageInfo? packageInfo;

  String? instagram = 'https://www.instagram.com/lionpremiummultimarcas';


  @override
  void initState() {
    super.initState();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1700),
    )..forward();
    cpfController.text = loginData.senha!;
    senhaController.text = loginData.cpf!;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: WillPopScope( 
        onWillPop: () async{
          return false;
        },
        child: SafeArea(
          child: GestureDetector(
            onTap: () {
              FocusScopeNode currentFocus = FocusScope.of(context);

              if (!currentFocus.hasPrimaryFocus) {
                currentFocus.unfocus();
              }
            },
            child: Container(
              padding: EdgeInsets.only(top: 0.0, left: 40.0, right: 40.0, bottom: 0.0),
              alignment: Alignment.center,
              child: Form(
                key: _formKey,
                child: CustomScrollView(
                  slivers: <Widget>[
                  SliverList(delegate: SliverChildListDelegate([

                    SizedBox(height: 50),

                    FadingSlidingWidget(
                      animationController: _animationController,
                      interval: const Interval(0.2, 0.9),
                      child: Container(
                        width: 60,
                        height: 60,
                        decoration: BoxDecoration(
                          color: Colors.black87,
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: AssetImage(
                              'assets/imgs/logo1.png',
                            )
                          )
                        ),
                      ),
                    ),
                  
              

                    SizedBox(height: 30),

                    Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(top: 40),
                      child: Text('Acesse sua conta', style: TextStyle(color: Colors.white, fontSize: 35, fontWeight: FontWeight.bold), textAlign: TextAlign.center),
                    ),

                    SizedBox(height: 20),

                    CustomInput.getInputLabel('CPF'),
                    //CPF
                    Container(
                      margin: EdgeInsets.only(left: 30, right: 30),
                      decoration: CustomInput.decorationCircular(),
                      child: TextFormField(
                        controller: cpfController,
                        inputFormatters: [
                          CnpjCpfFormatter(
                            eDocumentType: EDocumentType.CPF
                          )
                        ],
                        style: TextStyle(color: Colors.black, fontSize: 18.0),
                        decoration: CustomInput.inputDecorationI('Digite seu CPF', showError: true),
                        keyboardType: TextInputType.number,
                        textCapitalization: TextCapitalization.none,
                        textInputAction: TextInputAction.next,
                        cursorColor: Colors.black,
                        focusNode: _loginFocus,
                        validator: (text) {
                          if(text!.isEmpty) return "Insira o CPF";
                          if (CPF.isValid(text) == false)
                            return "CPF inválido";
                          return null;
                        },
                        onFieldSubmitted: (term){
                          _loginFocus.unfocus();
                          FocusScope.of(context).requestFocus(_senhaFocus);
                        },
                        onChanged: (val) =>
                          setState(() {
                            loginData.senha = val;
                        }),
                      ),
                    ),

                    SizedBox(height: 20),

                    CustomInput.getInputLabel('SENHA'),
                    //SENHA
                    Container(
                      margin: EdgeInsets.only(left: 30, right: 30),
                      decoration: CustomInput.decorationCircular(),
                      child: TextFormField(
                        maxLength: 9,
                        controller: senhaController,
                        style: TextStyle(color: Colors.black, fontSize: 18.0),
                        decoration: CustomInput.inputDecorationI('Digite sua senha', showError: true),
                        keyboardType: TextInputType.number,
                        obscureText: true,
                        textCapitalization: TextCapitalization.none,
                        textInputAction: TextInputAction.send,
                        cursorColor: Colors.black,
                        focusNode: _senhaFocus,
                        validator: (text) {
                          if(text!.length == 0) return "Insira uma senha";
                          return null;
                        },
                        onFieldSubmitted: (term){
                          _loginFocus.unfocus();
                          _senhaFocus.unfocus();

                          final form = _formKey.currentState;
                          
                          if (form!.validate()) {
                            form.save();
                            
                            _doLogin();
                          }
                          else {
                            _scaffoldKey.currentState!.showSnackBar(
                              SnackBar(
                                content: Text("Preencha o formulário corretamente!", style: TextStyle(fontSize: 18.0),),
                                backgroundColor: Colors.redAccent,
                                duration: Duration(seconds: 1),
                              )
                            );
                          }
                          
                        },
                        onChanged: (val) =>
                          setState(() {
                            loginData.cpf = val;
                        }),
                      ),
                    ),


                    SizedBox(height: 15),

                    GestureDetector(
                      child: Container(
                        alignment: Alignment.center,
                        margin: EdgeInsets.only(top: 40),
                        child: Text('Não consegue acessar a sua conta?\nClique aqui', style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w300), textAlign: TextAlign.center),
                      ),
                      onTap: () async {
                        
                      },
                    ),

                    //ENTRAR
                    Container(
                      margin: EdgeInsets.only(top: 30.0, left: 55, right: 55),
                      height: 50.0,
                      child: new RaisedButton(
                        color: CustomsColors.customOrange,
                        child: sending?CircularProgressIndicator(valueColor: new AlwaysStoppedAnimation<Color>(Colors.black)) : Text('Enviar', style: TextStyle(color: Colors.black, fontSize: 22.0, fontWeight: FontWeight.bold)),
                        shape: new RoundedRectangleBorder(borderRadius: new BorderRadius.circular(10.0)),
                        onPressed: !sending?() {

                          final form = _formKey.currentState;
                          
                          if (form!.validate()) {
                            form.save();
                            
                            _doLogin();
                          }
                          else {
                            _scaffoldKey.currentState!.showSnackBar(
                              SnackBar(
                                content: Text("Preencha o formulário corretamente!", style: TextStyle(fontSize: 18.0)),
                                backgroundColor: Colors.redAccent,
                                duration: Duration(seconds: 1),
                              )
                            );
                          }
                          
                        }:null,
                      )
                    )

              
                  ]))
                ])
              ),
              decoration: BoxDecoration(
                color: Colors.white30,
                image: DecorationImage(
                  image: AssetImage('assets/imgs/intro1.jpg'),
                  fit: BoxFit.cover,
                  colorFilter: ColorFilter.mode(Colors.black54, BlendMode.darken)
                )
              ),
            )
          )
        ),
      ),
      bottomNavigationBar: Container(
        height: MediaQuery.of(context).size.height * 0.1,
        width: MediaQuery.of(context).size.width,
        color: Colors.black,
        alignment: Alignment.center,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            TextButton(
              style: TextButton.styleFrom(
                minimumSize: Size.zero,
                padding: EdgeInsets.zero, 
              ),
              child: Container(
                height: 30,
                width: 30,
                child: Icon(FontAwesomeIcons.linkedinIn, size: 20, color: Colors.black),
                decoration: BoxDecoration(
                  color: CustomsColors.customOrange,
                  borderRadius: BorderRadius.all(Radius.circular(10))
                ),
              ),
              onPressed: (){

              },
            ),
            TextButton(
              style: TextButton.styleFrom(
                minimumSize: Size.zero,
                padding: EdgeInsets.zero, 
              ),
              child: Container(
                height: 30,
                width: 30,
                child: Icon(FontAwesomeIcons.instagram, size: 20, color: Colors.black),
                decoration: BoxDecoration(
                  color: CustomsColors.customOrange,
                  borderRadius: BorderRadius.all(Radius.circular(10))
                ),
              ),
              onPressed: () async {
                await canLaunch(instagram!) ? await launch(instagram!) : throw '';
              },
            ),
            TextButton(
              style: TextButton.styleFrom(
                minimumSize: Size.zero,
                padding: EdgeInsets.zero, 
              ),
              child: Container(
                height: 30,
                width: 30,
                child: Icon(FontAwesomeIcons.youtube, size: 20, color: Colors.black),
                decoration: BoxDecoration(
                  color: CustomsColors.customOrange,
                  borderRadius: BorderRadius.all(Radius.circular(10))
                ),
              ),
              onPressed: (){

              },
            ),
          ],
        ),
      ),
    );
  }


  void _doLogin(){
    setState(() {
      sending = true;
    });
    Future.delayed(Duration(seconds: 1)).then((value){
      Navigator.of(context).push(MaterialPageRoute(
        builder: (BuildContext context) => HomePage())
      );
    });
    
    // setState(() {
    //   //loginData.email = "menezesneto13@hotmail.com";
    //   sending = true;
    // });

    // UserProvider.doLogin(loginData, infoData).then((value) {
    //   setState(() {
    //     sending = false;
    //      if (value['value'] == true) {
    //       final pageCtlr = GetIt.I.get<TabPageController>();
    //       pageCtlr.setPageIndex(1);
    //       if(pageCtlr.pageController != null) pageCtlr.pageController!.jumpToPage(1);
    //       Navigator.of(context).pushReplacement(MaterialPageRoute(
    //         builder: (BuildContext context) => TabPage())
    //       );
    //      } 
    //      else CustomInput.showSnackbarErrorMessage(_scaffoldKey, value['msgRetorno']);
    //   });
    // });
  }
 
}
