import 'dart:async';
import 'package:lionpremium/pages/home_page.dart';
import 'package:package_info/package_info.dart';
import 'package:lionpremium/models/usuario_model.dart';
import 'package:flutter/material.dart';
import 'package:lionpremium/resources/app_config.dart';

import '../providers/api_provider.dart';
import '../providers/user_provider.dart';
import 'intro/intro_page.dart';

class SplashPage extends StatefulWidget {
  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {

  bool isAnimatedImg = false;
  bool isAnimatedText = false;
  PackageInfo? packageInfo;

  @override
  void initState() {
    super.initState();
    PackageInfo.fromPlatform().then((value){
      packageInfo = value;
    });
    Future.delayed(const Duration(seconds: 2), () {
      setState(() {
        isAnimatedText = true;
      });

      Future.delayed(const Duration(milliseconds: 1000), () {
        setState(() {
          isAnimatedImg = true;
        });

        Future.delayed(const Duration(milliseconds: 100), () async {
          checkIsLogin();
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    ApiProvider.setUrlServer(AppConfig.of(context)!.urlServer);
    ApiProvider.setBuildType(AppConfig.of(context)!.buildType);
    return Scaffold(
      body: Container(
        color: Colors.white,
        alignment: Alignment.center,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: AnimatedSwitcher(
          duration: const Duration(milliseconds: 500),
          transitionBuilder: (Widget child, Animation<double> animation) {
            return ScaleTransition(child: child, scale: animation);
          },
          child: ListView(
            primary: false,
            shrinkWrap: true,
            children: <Widget>[
              Hero(
                tag: 'splash',
                child: Container(
                  height: MediaQuery.of(context).size.width / 2,
                  alignment: Alignment.center,
                  child: Image.asset(
                    "assets/imgs/logo1.png",
                    height: MediaQuery.of(context).size.width / 2,
                  ),
                )
              ),

              Container(
                alignment: Alignment.center,
                margin: EdgeInsets.only(top: 15),
                child: AnimatedOpacity(
                  opacity: isAnimatedText && !isAnimatedImg ? 1.0 : 0.0,
                  duration: Duration(milliseconds: 500),
                  child: FutureBuilder(
                   future: UserProvider.getAlunoLocal(),
                    builder: (context, snapCustomer) {
                      return Text(
                        'Comércio e Locação de Veículos',
                        style: TextStyle(color: Colors.black87, fontSize: 18.0, fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      );
                    }
                  )
                ),
              )
            ],
          ),
        )
      )
    );
  }

  Future<Null> checkIsLogin() async {
    Future.delayed(Duration(seconds: 1)).then((value) async {

      UsuarioModel? aluno = await UserProvider.getAlunoLocal();
      String? versaoAppLocal = await UserProvider.getVersionAppLocal();
      String? versaoApp = packageInfo!.version.replaceAll(".", "");

      if(aluno != null) {
        if(versaoAppLocal == null){
          UserProvider.setUserLocal(aluno);
          UserProvider.setVersionAppLocal(versaoApp);
          UserProvider.getAlunoLocal();

          Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => HomePage()),
          );
        }else{
          if(versaoAppLocal == versaoApp){
            
            UserProvider.setUserLocal(aluno);
            UserProvider.setVersionAppLocal(versaoApp);
            UserProvider.getAlunoLocal();

            Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => HomePage()),
            );
          }else{
            UserProvider.logoutUser();
            Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => IntroPage()),
            );
          }
        }
        
      } 
      else {
        Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => IntroPage()),
        );
      }
      
      print("versao do app local $versaoAppLocal");
      print("versao do app $versaoApp");
      print("aluno $aluno");
    });
  }
  
}
