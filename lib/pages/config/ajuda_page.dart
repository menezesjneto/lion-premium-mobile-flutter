import 'package:get_it/get_it.dart';
import 'package:lionpremium/controllers/gps_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:lionpremium/pages/home_page.dart';
import 'package:lionpremium/pages/intro/sliding_widget.dart';
import 'package:lionpremium/widgets/custom_text/custom_text.dart';
import 'package:lionpremium/widgets/menu/menu_page.dart';

class AjudaPage extends StatefulWidget {
  @override
  _AjudaPageState createState() => _AjudaPageState();
}

class _AjudaPageState extends State<AjudaPage> with SingleTickerProviderStateMixin  {
  
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  AnimationController? _animationController;

  final gpsCtlr = GetIt.I.get<GpsController>();

  int? tabIndex = 0;

  @override
  void initState() {
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 1700),
    )..forward();
    super.initState();
  }

  @override
  bool get wantKeepAlive => true;
 

  @override
  Widget build(BuildContext context) {
    var height = MediaQuery.of(context).size.height;
    var width = MediaQuery.of(context).size.width;
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.grey[200],
      body: CustomScrollView(
        slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate(
              [
                
                _appBar(height, width),

                Container(
                  alignment: Alignment.centerLeft,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      IconButton(
                        onPressed: (){
                          Navigator.pop(context);
                        },
                        icon: Icon(FontAwesomeIcons.chevronCircleLeft, color: Colors.grey[500]),
                      ),
                      Spacer(),
                      Container(
                        child: CustomText(text: 'Ajuda', fontWeight: FontWeight.bold, color: Colors.black, fontSize: 18),
                      ),
                      Spacer(),
                      IconButton(
                        onPressed: (){},
                        icon: Icon(FontAwesomeIcons.chevronCircleLeft, color: Colors.transparent),
                      ),
                    ],
                  ),
                ),

                Container(
                  width: width,
                  height: 180,
                  margin: EdgeInsets.all(20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(FontAwesomeIcons.questionCircle, size: 60),
                      SizedBox(height: 15),
                      CustomText(text: 'F.A.Q', fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold),
                    ],
                  ),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10))
                  ),
                ),

                SizedBox(height: 20),

                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      width: width * 0.41,
                      height: 180,
                      margin: EdgeInsets.only(left: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(FontAwesomeIcons.whatsapp, size: 60),
                          SizedBox(height: 15),
                          CustomText(text: 'Telefone', fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold),
                        ],
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10))
                      ),
                    ),

                    Container(
                      width: width * 0.41,
                      height: 180,
                      margin: EdgeInsets.only(right: 20),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Icon(FontAwesomeIcons.envelopeOpen, size: 60),
                          SizedBox(height: 15),
                          CustomText(text: 'Email', fontSize: 16, color: Colors.black, fontWeight: FontWeight.bold),
                        ],
                      ),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.all(Radius.circular(10))
                      ),
                    ),

                  ],
                ),
                

              ]
            )
          )
        ]
      )
    );
  }

  Widget _appBar(height, width){
    return Container(
      height: height * 0.15,
      width: width,
      alignment: Alignment.bottomCenter,
      padding: EdgeInsets.only(top: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Container(
            height: height * 0.1,
            width: width * 0.65,
            color: Colors.yellow,
            child: FlutterLogo(),
          ),
          VerticalDivider(color: Colors.transparent),
          Container(
            child: TextButton(
              child: Icon(Icons.menu, color: Colors.black, size: 60),
              onPressed: (){
                Navigator.of(context).push(PageRouteBuilder(
                  opaque: false,
                  pageBuilder: (BuildContext context, _, __) => MenuPage()));
              },
            ),
          ),
        ],
      ),
    );
  }
}
