import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get_it/get_it.dart';
import 'package:lionpremium/controllers/cliente_controller.dart';
import 'package:lionpremium/controllers/coordenadas_controller.dart';
import 'package:lionpremium/controllers/gps_controller.dart';
import 'package:lionpremium/controllers/questionario_controller.dart';
import 'package:lionpremium/controllers/tab_page_controller.dart';
import 'package:lionpremium/resources/app_config.dart';
import 'main.dart';
import 'services/navigation_service.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  setupLocator();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  GetIt getIt = GetIt.I;
  getIt.registerSingleton<GpsController>((GpsController()));
  getIt.registerSingleton<TabPageController>((TabPageController()));
  getIt.registerSingleton<QuestionarioController>((QuestionarioController()));
  getIt.registerSingleton<CoordenadasController>((CoordenadasController()));
  getIt.registerSingleton<ClienteController>((ClienteController()));

  // var configureApp = AppConfig(
  //   buildType: 'dev',
  //   child: materialApp,
  //   urlServer: '201.33.25.40:33',
  // );
  
  var configureApp = AppConfig(
    buildType: 'dev',
    child: materialApp,
    urlServer: '201.33.21.62',
  );


  return runApp(configureApp);
}

