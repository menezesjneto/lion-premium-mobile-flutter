import 'package:lionpremium/pages/not_found_page.dart';
import 'package:lionpremium/routes/routes.dart';
import 'package:lionpremium/widgets/theme.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

final materialApp = MaterialApp(
  title: "lionpremium",
  theme: ThemeData(
    primarySwatch: CustomsColors.customOrange,
    primaryColor: Colors.white,
    textSelectionColor: Colors.grey[400],
  ),
  debugShowCheckedModeBanner: false,
  localizationsDelegates: [
    GlobalCupertinoLocalizations.delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
  ],
  routes: routesApp,
  builder: (context, child) {
    return MediaQuery(
      child: child!,
      data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
    );
  }
);

Route<dynamic> generateRoute(RouteSettings settings) {
  switch (settings.name) {
    default:
      return MaterialPageRoute(builder: (context) => NotFoundPage());
  }
}
